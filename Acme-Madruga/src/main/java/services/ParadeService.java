
package services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ParadeRepository;
import domain.Actor;
import domain.Brotherhood;
import domain.Member;
import domain.Message;
import domain.Parade;
import domain.Request;

@Service
@Transactional
public class ParadeService {

	// Manage Repository
	@Autowired
	private ParadeRepository	paradeRepository;

	// Supporting services
	@Autowired
	private ActorService		actorService;

	@Autowired
	private MessageService		messageService;

	@Autowired
	private MemberService		memberService;

	@Autowired
	private BrotherhoodService	brotherhoodService;

	@Autowired
	private RequestService		requestService;

	// Validator
	@Autowired(required = false)
	private Validator			validator;


	/************************************* CRUD methods ********************************/

	public Parade create() {

		Parade result;

		result = new Parade();

		result.setTicker(this.generateTicker());
		while (this.isValidTicker(result.getTicker()) == false)
			result.setTicker(this.generateTicker());

		final List<Request> requests = new ArrayList<>();
		result.setRequests(requests);

		return result;
	}

	public Parade findOne(final int id) {
		final Parade result = this.paradeRepository.findOne(id);

		Assert.notNull(result);

		return result;
	}

	@Transactional
	public Collection<Parade> findAll() {
		final Collection<Parade> result = this.paradeRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Parade save(final Parade parade) {
		//System.out.println("save");
		Assert.notNull(parade);
		Actor principal;

		// Principal must be a Brotherhood
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Brotherhood.class, principal);

		final Brotherhood brotherhood = (Brotherhood) principal;

		if (parade.getId() == 0)
			parade.setBrotherhood(brotherhood);
		//this.automaticNotification(parade);
		else
			Assert.isTrue(brotherhood.getParades().contains(parade));

		final Parade p = this.paradeRepository.save(parade);

		return p;
	}

	public void delete(final Parade parade) {
		Assert.notNull(parade);
		Actor principal;

		// Principal must be a Brotherhood

		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Brotherhood.class, principal);
		Assert.isTrue(parade.getId() != 0);
		Assert.isTrue(parade.getDraftMode());

		final Brotherhood brotherhood = (Brotherhood) principal;

		//Borramos sus requests
		//for (final Request r : parade.getRequests())
		//this.requestService.delete(r.getId());

		//La borramos de su hermandad
		brotherhood.getParades().remove(parade);

		this.paradeRepository.delete(parade);

	}
	/************************************* Reconstruct ******************************************/
	public Parade reconstruct(final Parade pruned, final BindingResult binding) {
		Parade result = this.create();
		Parade temp;

		if (pruned.getId() == 0) {
			this.validator.validate(pruned, binding);
			result = pruned;
		} else {
			temp = this.findOne(pruned.getId());
			result.setId(temp.getId());
			result.setVersion(temp.getVersion());
			result.setBrotherhood(temp.getBrotherhood());
			result.setTicker(temp.getTicker());

			result.setTitle(pruned.getTitle());
			result.setDescription(pruned.getDescription());
			result.setMoment(pruned.getMoment());
			result.setDraftMode(pruned.getDraftMode());

			this.validator.validate(result, binding);
		}

		return result;
	}

	/************************************* Other business methods ********************************/
	public String generateTicker() {
		String ticker = "";
		final DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
		final Date date = new Date();
		final String tickerDate = (dateFormat.format(date));
		final String tickerAlphanumeric = RandomStringUtils.randomAlphanumeric(5).toUpperCase();
		ticker = ticker.concat(tickerDate).concat("-").concat(tickerAlphanumeric);
		return ticker;
	}

	public void flush() {
		this.paradeRepository.flush();
	}

	private void automaticNotification(final Parade parade) {
		final Message message = this.messageService.create();
		message.setBody("The brotherhood " + parade.getBrotherhood().getTitle() + " has published a procession called " + parade.getTitle() + ".");

		message.setIsNotification(true);
		for (final Member m : this.memberService.findByBrotherhood(parade.getBrotherhood())) {
			message.getMessageBoxes().add(m.getMessageBox("in"));
			message.getRecipients().add(m);
		}
		message.setPriority("MEDIUM");
		message.setSubject("New procession by " + parade.getBrotherhood().getTitle());

		final Message send = this.messageService.save(message);

		for (final Member m : this.memberService.findByBrotherhood(parade.getBrotherhood()))
			m.getMessageBox("in").addMessage(send);
	}

	public Boolean isValidTicker(final String ticker) {
		Boolean res = true;
		final Collection<String> tickers = new ArrayList<String>();
		for (final Parade p : this.paradeRepository.findAll())
			tickers.add(p.getTicker());

		if (tickers.contains(ticker))
			res = false;
		return res;
	}

	public Collection<Parade> findByBrotherhood(final Brotherhood brotherhood) {
		final Collection<Parade> result = this.paradeRepository.findByBrotherhood(brotherhood.getId());
		Assert.notNull(result);
		return result;
	}

}
