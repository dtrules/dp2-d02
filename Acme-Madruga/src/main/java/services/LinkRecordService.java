
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.LinkRecordRepository;
import domain.Actor;
import domain.Brotherhood;
import domain.History;
import domain.LinkRecord;

@Service
@Transactional
public class LinkRecordService {

	// Manage Repository

	@Autowired
	private LinkRecordRepository	linkRecordRepository;

	// Supporting services
	@Autowired
	private ActorService			actorService;

	@Autowired
	private BrotherhoodService		brotherhoodService;


	/************************************* CRUD methods ********************************/
	public LinkRecord create() {
		LinkRecord result;
		Actor principal;

		result = new LinkRecord();

		// Principal must be a Brotherhood
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Brotherhood.class, principal);

		return result;
	}

	public LinkRecord findOne(final int id) {
		final LinkRecord result = this.linkRecordRepository.findOne(id);

		Assert.notNull(result);

		return result;
	}

	public Collection<LinkRecord> findAll() {
		final Collection<LinkRecord> result = this.linkRecordRepository.findAll();

		Assert.notNull(result);

		return result;
	}

	public LinkRecord save(final LinkRecord linkRecord) {
		Assert.notNull(linkRecord);
		Actor principal;

		// Principal must be a Brotherhood
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Brotherhood.class, principal);

		this.checkLinkRecord(linkRecord);

		final LinkRecord lr = this.linkRecordRepository.save(linkRecord);

		final History his = this.brotherhoodService.findByPrincipal().getHistory();

		if (linkRecord.getId() == 0 && his.getLinkRecords().contains(linkRecord) == false)
			his.getLinkRecords().add(lr);

		if (linkRecord.getId() != 0)
			Assert.isTrue(his.getLinkRecords().contains(linkRecord));

		return lr;

	}

	public void delete(final int linkRecordId) {

		Actor principal;

		final LinkRecord p = this.findOne(linkRecordId);

		Assert.notNull(p);

		// Principal must be a Brotherhood
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Brotherhood.class, principal);

		final History h = this.brotherhoodService.findByPrincipal().getHistory();

		Assert.isTrue(h.getLinkRecords().contains(p));

		h.getLinkRecords().remove(p);
		this.linkRecordRepository.delete(p);
	}

	public void flush() {
		this.linkRecordRepository.flush();
	}

	/************************************* Other business methods ********************************/

	public void checkLinkRecord(final LinkRecord linkRecord) {
		boolean check = true;

		if (linkRecord.getText() == null || linkRecord.getTitle() == null || linkRecord.getURL() == null)
			check = false;

		Assert.isTrue(check);
	}
}
