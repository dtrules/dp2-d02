
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.HistoryRepository;
import security.Authority;
import domain.History;
import domain.InceptionRecord;
import domain.LegalRecord;
import domain.LinkRecord;
import domain.MiscellaneousRecord;
import domain.PeriodRecord;

@Service
@Transactional
public class HistoryService {

	// Manage Repository

	@Autowired(required = false)
	private HistoryRepository		historyRepository;

	// Supporting services
	@Autowired
	private ActorService			actorService;

	@Autowired
	private BrotherhoodService		brotherhoodService;

	@Autowired
	private InceptionRecordService	inceptionRecordService;


	// CRUD methods

	public History create() {
		final History result = new History();

		final Collection<PeriodRecord> periodRecords = new ArrayList<PeriodRecord>();
		final Collection<LegalRecord> legalRecords = new ArrayList<LegalRecord>();
		final Collection<LinkRecord> linkRecords = new ArrayList<LinkRecord>();
		final Collection<MiscellaneousRecord> miscellaneousRecords = new ArrayList<MiscellaneousRecord>();

		final InceptionRecord inceptionRecord = this.inceptionRecordService.create();
		final InceptionRecord saved = this.inceptionRecordService.saveRepo(inceptionRecord);
		result.setInceptionRecord(saved);
		result.setPeriodRecords(periodRecords);
		result.setLegalRecords(legalRecords);
		result.setLinkRecords(linkRecords);
		result.setMiscellaneousRecords(miscellaneousRecords);

		return result;
	}

	public History findOne(final int historyId) {
		final History result = this.historyRepository.findOne(historyId);
		Assert.notNull(result);

		return result;
	}

	public Collection<History> findAll() {
		final Collection<History> result = this.historyRepository.findAll();
		Assert.notNull(result);
		Assert.notEmpty(result);

		return result;
	}

	public History save(final History history) {
		Assert.isTrue(this.actorService.findByPrincipal().getUserAccount().getAuthorities().contains(Authority.BROTHERHOOD));
		Assert.notNull(history);
		Assert.notNull(history.getInceptionRecord());
		final History result = this.historyRepository.save(history);
		this.brotherhoodService.findByPrincipal().setHistory(result);
		this.brotherhoodService.save(this.brotherhoodService.findByPrincipal());
		return result;
	}

	public void delete(final History history) {
		Assert.isTrue(this.actorService.findByPrincipal().getUserAccount().getAuthorities().contains(Authority.BROTHERHOOD));
		Assert.notNull(history);

		this.historyRepository.delete(history);
	}

	public History saveRepo(final History history) {
		return this.historyRepository.save(history);
	}

	/************************************* Other business methods ********************************/

}
