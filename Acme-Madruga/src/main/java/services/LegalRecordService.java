
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.LegalRecordRepository;
import domain.Actor;
import domain.Brotherhood;
import domain.History;
import domain.LegalRecord;

@Service
@Transactional
public class LegalRecordService {

	// Manage Repository

	@Autowired
	private LegalRecordRepository	legalRecordRepository;

	// Supporting services
	@Autowired
	private ActorService			actorService;

	@Autowired
	private BrotherhoodService		brotherhoodService;


	/************************************* CRUD methods ********************************/
	public LegalRecord create() {
		LegalRecord result;

		result = new LegalRecord();

		return result;
	}

	public LegalRecord findOne(final int id) {
		final LegalRecord result = this.legalRecordRepository.findOne(id);

		Assert.notNull(result);

		return result;
	}

	public Collection<LegalRecord> findAll() {
		final Collection<LegalRecord> result = this.legalRecordRepository.findAll();

		Assert.notNull(result);

		return result;
	}

	public LegalRecord save(final LegalRecord legalRecord) {
		Assert.notNull(legalRecord);
		Actor principal;

		// Principal must be a Brotherhood
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Brotherhood.class, principal);

		this.checkLegalRecord(legalRecord);

		final LegalRecord lr = this.legalRecordRepository.save(legalRecord);

		final History his = this.brotherhoodService.findByPrincipal().getHistory();

		if (legalRecord.getId() == 0 && his.getLegalRecords().contains(legalRecord) == false)
			his.getLegalRecords().add(lr);

		if (legalRecord.getId() != 0)
			Assert.isTrue(his.getLegalRecords().contains(legalRecord));

		return lr;

	}

	public void delete(final int legalRecordId) {

		Actor principal;

		final LegalRecord p = this.findOne(legalRecordId);

		Assert.notNull(p);

		// Principal must be a Brotherhood
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Brotherhood.class, principal);

		final History h = this.brotherhoodService.findByPrincipal().getHistory();

		Assert.isTrue(h.getLegalRecords().contains(p));

		h.getLegalRecords().remove(p);
		this.legalRecordRepository.delete(p);
	}

	public void flush() {
		this.legalRecordRepository.flush();
	}

	/************************************* Other business methods ********************************/

	public void checkLegalRecord(final LegalRecord legalRecord) {
		boolean check = true;

		if (legalRecord.getText() == null || legalRecord.getTitle() == null || legalRecord.getLegalName() == null || legalRecord.getApplicableLaws() == null)
			check = false;

		Assert.isTrue(check);
	}
}
