
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.MiscellaneousRecordRepository;
import domain.Actor;
import domain.Brotherhood;
import domain.History;
import domain.MiscellaneousRecord;

@Service
@Transactional
public class MiscellaneousRecordService {

	// Manage Repository

	@Autowired
	private MiscellaneousRecordRepository	miscellaneousRecordRepository;

	// Supporting services
	@Autowired
	private ActorService					actorService;

	@Autowired
	private BrotherhoodService				brotherhoodService;


	/************************************* CRUD methods ********************************/
	public MiscellaneousRecord create() {
		MiscellaneousRecord result;
		Actor principal;

		result = new MiscellaneousRecord();

		// Principal must be a Brotherhood
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Brotherhood.class, principal);

		return result;
	}

	public MiscellaneousRecord findOne(final int id) {
		final MiscellaneousRecord result = this.miscellaneousRecordRepository.findOne(id);

		Assert.notNull(result);

		return result;
	}

	public Collection<MiscellaneousRecord> findAll() {
		final Collection<MiscellaneousRecord> result = this.miscellaneousRecordRepository.findAll();

		Assert.notNull(result);

		return result;
	}

	public MiscellaneousRecord save(final MiscellaneousRecord miscellaneousRecord) {
		Assert.notNull(miscellaneousRecord);
		Actor principal;

		// Principal must be a Brotherhood
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Brotherhood.class, principal);

		this.checkMiscellaneousRecord(miscellaneousRecord);

		final MiscellaneousRecord mr = this.miscellaneousRecordRepository.save(miscellaneousRecord);

		final History his = this.brotherhoodService.findByPrincipal().getHistory();

		if (miscellaneousRecord.getId() == 0 && his.getMiscellaneousRecords().contains(mr) == false)
			his.getMiscellaneousRecords().add(mr);

		if (miscellaneousRecord.getId() != 0)
			Assert.isTrue(his.getMiscellaneousRecords().contains(miscellaneousRecord));

		return mr;

	}

	public void delete(final int miscellaneousRecordId) {

		Actor principal;

		final MiscellaneousRecord p = this.findOne(miscellaneousRecordId);

		Assert.notNull(p);

		// Principal must be a Brotherhood
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Brotherhood.class, principal);

		final History h = this.brotherhoodService.findByPrincipal().getHistory();

		Assert.isTrue(h.getMiscellaneousRecords().contains(p));

		h.getMiscellaneousRecords().remove(p);
		this.miscellaneousRecordRepository.delete(p);
	}

	public void flush() {
		this.miscellaneousRecordRepository.flush();
	}

	/************************************* Other business methods ********************************/

	public void checkMiscellaneousRecord(final MiscellaneousRecord miscellaneousRecord) {
		boolean check = true;

		if (miscellaneousRecord.getText() == null || miscellaneousRecord.getTitle() == null)
			check = false;

		Assert.isTrue(check);
	}
}
