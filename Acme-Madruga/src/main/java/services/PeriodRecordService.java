
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.PeriodRecordRepository;
import domain.Actor;
import domain.Brotherhood;
import domain.History;
import domain.PeriodRecord;
import domain.Url;

@Service
@Transactional
public class PeriodRecordService {

	// Manage Repository

	@Autowired
	private PeriodRecordRepository	periodRecordRepository;

	// Supporting services
	@Autowired
	private ActorService			actorService;

	@Autowired
	private BrotherhoodService		brotherhoodService;


	/************************************* CRUD methods ********************************/
	public PeriodRecord create() {
		PeriodRecord result;
		Actor principal;

		result = new PeriodRecord();

		// Principal must be a Brotherhood
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Brotherhood.class, principal);

		final List<Url> pictures = new ArrayList<Url>();

		result.setPictures(pictures);

		return result;
	}

	public PeriodRecord findOne(final int id) {
		final PeriodRecord result = this.periodRecordRepository.findOne(id);

		//Assert.notNull(result);

		return result;
	}

	public Collection<PeriodRecord> findAll() {
		final Collection<PeriodRecord> result = this.periodRecordRepository.findAll();
		//Assert.notNull(result);

		return result;
	}

	public PeriodRecord save(final PeriodRecord periodRecord) {
		Assert.notNull(periodRecord);
		Actor principal;

		// Principal must be a Brotherhood
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Brotherhood.class, principal);

		this.checkPeriodRecord(periodRecord);

		final PeriodRecord pr = this.periodRecordRepository.save(periodRecord);

		final History his = this.brotherhoodService.findByPrincipal().getHistory();

		if (periodRecord.getId() == 0 && his.getPeriodRecords().contains(pr) == false)
			his.getPeriodRecords().add(pr);

		if (periodRecord.getId() != 0)
			Assert.isTrue(his.getPeriodRecords().contains(periodRecord));

		return pr;

	}

	public void delete(final int periodRecordId) {

		Actor principal;

		final PeriodRecord p = this.findOne(periodRecordId);

		Assert.notNull(p);

		// Principal must be a Brotherhood
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Brotherhood.class, principal);

		final History h = this.brotherhoodService.findByPrincipal().getHistory();

		Assert.isTrue(h.getPeriodRecords().contains(p));

		h.getPeriodRecords().remove(p);
		this.periodRecordRepository.delete(p);
	}

	public void flush() {
		this.periodRecordRepository.flush();
	}

	/************************************* Other business methods ********************************/

	public void checkPeriodRecord(final PeriodRecord periodRecord) {
		boolean check = true;

		if (periodRecord.getText() == null || periodRecord.getTitle() == null)
			check = false;

		Assert.isTrue(periodRecord.getEndYear() >= periodRecord.getStartYear());

		Assert.isTrue(check);
	}
}
