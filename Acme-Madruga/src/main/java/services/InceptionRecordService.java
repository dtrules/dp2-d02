
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.Validator;

import repositories.InceptionRecordRepository;
import domain.Actor;
import domain.Brotherhood;
import domain.InceptionRecord;
import domain.Url;

@Service
@Transactional
public class InceptionRecordService {

	// Manage Repository

	@Autowired
	private InceptionRecordRepository	inceptionRecordRepository;

	// Supporting services
	@Autowired
	private ActorService				actorService;

	@Autowired
	private BrotherhoodService			brotherhoodService;

	// Validator
	@Autowired(required = false)
	private Validator					validator;


	/************************************* CRUD methods ********************************/
	public InceptionRecord create() {
		InceptionRecord result;

		result = new InceptionRecord();

		// Principal must be a Brotherhood

		result.setPictures(new ArrayList<Url>());

		return result;
	}

	public InceptionRecord findOne(final int id) {

		final InceptionRecord result = this.inceptionRecordRepository.findOne(id);

		//Assert.notNull(result);

		return result;
	}

	public Collection<InceptionRecord> findAll() {
		final Collection<InceptionRecord> result = this.inceptionRecordRepository.findAll();
		//Assert.notNull(result);

		return result;
	}

	public InceptionRecord save(final InceptionRecord inceptionRecord) {
		Assert.notNull(inceptionRecord);
		Actor principal;

		// Principal must be a Brotherhood
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Brotherhood.class, principal);

		this.checkInceptionRecord(inceptionRecord);

		final InceptionRecord ir = this.inceptionRecordRepository.save(inceptionRecord);

		this.brotherhoodService.findByPrincipal().getHistory().setInceptionRecord(ir);

		return ir;

	}

	public void flush() {
		this.inceptionRecordRepository.flush();
	}

	/************************************* Other business methods ********************************/

	public void checkInceptionRecord(final InceptionRecord inceptionRecord) {
		boolean check = true;

		if (inceptionRecord.getText() == null || inceptionRecord.getTitle() == null || inceptionRecord.getPictures() == null)
			check = false;

		Assert.isTrue(check);
	}

	public InceptionRecord saveRepo(final InceptionRecord inceptionRecord) {
		inceptionRecord.setTitle("Complete Title");
		inceptionRecord.setText("Temporal Text");
		final Collection<Url> urls = new ArrayList<Url>();
		inceptionRecord.setPictures(urls);
		return this.inceptionRecordRepository.save(inceptionRecord);
	}
}
