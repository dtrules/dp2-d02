
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
public class LinkRecord extends DomainEntity {

	// Attributes

	private String	title;
	private String	text;
	private String	URL;


	// Relationships --------------------

	// Getters & setters
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getText() {
		return this.text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	@URL
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getURL() {
		return this.URL;
	}

	public void setURL(final String URL) {
		this.URL = URL;
	}

	// RELATIONSHIPS	---------------------------------------

	// To String ------------------------------------------------

	@Override
	public String toString() {
		return "Link Record [Title = " + this.getTitle() + "]";
	}

}
