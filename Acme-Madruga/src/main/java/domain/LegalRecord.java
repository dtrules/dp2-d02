
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

@Entity
@Access(AccessType.PROPERTY)
public class LegalRecord extends DomainEntity {

	// Attributes

	private String	title;
	private String	text;
	private String	legalName;
	private int		VAT;
	private String	applicableLaws;


	// Relationships --------------------

	// Getters & setters
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getText() {
		return this.text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getLegalName() {
		return this.legalName;
	}

	public void setLegalName(final String legalName) {
		this.legalName = legalName;
	}

	@NotNull
	@Min(0)
	public int getVAT() {
		return this.VAT;
	}

	public void setVAT(final int VAT) {
		this.VAT = VAT;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getApplicableLaws() {
		return this.applicableLaws;
	}

	public void setApplicableLaws(final String applicableLaws) {
		this.applicableLaws = applicableLaws;
	}

	// RELATIONSHIPS	---------------------------------------

	// To String ------------------------------------------------

	@Override
	public String toString() {
		return "Legal Record [Title = " + this.getTitle() + "]";
	}

}
