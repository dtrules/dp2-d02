
package controllers.brotherhood;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.BrotherhoodService;
import services.PeriodRecordService;
import controllers.AbstractController;
import domain.PeriodRecord;
import domain.Url;

@Controller
@RequestMapping("/periodRecord/brotherhood")
public class PeriodRecordBrotherhoodController extends AbstractController {

	@Autowired
	private PeriodRecordService	periodRecordService;

	@Autowired
	private BrotherhoodService	brotherhoodService;


	@ExceptionHandler(TypeMismatchException.class)
	public ModelAndView handleMismatchException(final TypeMismatchException oops) {
		return new ModelAndView("redirect:/");
	}

	// List ------------------------------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView result;
		Collection<PeriodRecord> periodRecords = new ArrayList<PeriodRecord>();

		try {
			periodRecords = this.brotherhoodService.findByPrincipal().getHistory().getPeriodRecords();

			result = new ModelAndView("periodRecord/brotherhood/list");
			result.addObject("periodRecords", periodRecords);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	// Create ------------------------------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		PeriodRecord periodRecord;

		periodRecord = this.periodRecordService.create();

		result = this.createEditModelAndView(periodRecord);

		return result;
	}

	// Save the period record ------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final PeriodRecord periodRecord, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = this.createEditModelAndView(periodRecord);
		} else
			try {
				this.periodRecordService.save(periodRecord);
				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				System.out.println(periodRecord);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.createEditModelAndView(periodRecord);

				if (oops instanceof DataIntegrityViolationException)
					result = this.createEditModelAndView(periodRecord, "periodRecord.commit.username");
				else
					result = this.createEditModelAndView(periodRecord, "periodRecord.commit.error");
			}
		return result;
	}

	// Edit ------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int periodRecordId) {
		ModelAndView result;
		PeriodRecord periodRecord;

		try {
			periodRecord = this.periodRecordService.findOne(periodRecordId);
			Assert.notNull(periodRecord);
		} catch (final Throwable oops) {
			result = this.forbiddenOpperation();
			return result;
		}

		result = this.createEditModelAndView(periodRecord);

		return result;
	}

	// Delete --------------------------------------------------------------------------------------
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int periodRecordId) {
		ModelAndView result;

		try {
			this.periodRecordService.delete(periodRecordId);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = this.forbiddenOpperation();
			return result;
		}

		return result;
	}

	// Picture  ------------------------------------------------------------------------------------
	@RequestMapping(value = "/addPicture", method = RequestMethod.GET)
	public ModelAndView addPicture(@RequestParam final int periodRecordId) {
		ModelAndView result;
		final Url url;

		try {
			url = new Url();
			result = new ModelAndView("periodRecord/brotherhood/addPicture");
			result.addObject("url", url);
			result.addObject("periodRecordId", periodRecordId);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	@RequestMapping(value = "/deletePicture", method = RequestMethod.GET)
	public ModelAndView deletePicture(@RequestParam final String link, @RequestParam final int periodRecordId) {
		ModelAndView result = null;
		try {
			final PeriodRecord periodRecord = this.periodRecordService.findOne(periodRecordId);
			for (final Url picture : periodRecord.getPictures())
				if (picture.getLink().equals(link)) {
					periodRecord.getPictures().remove(picture);
					this.periodRecordService.save(periodRecord);
					result = new ModelAndView("redirect:../list.do");
					break;
				}
			if (result == null)
				result = this.createEditModelAndView(periodRecord);

		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	// SAVE ------------------------------------------------------------------------------------
	@RequestMapping(value = "/addPicture", method = RequestMethod.POST, params = "save")
	public ModelAndView savePicture(@RequestParam final int periodRecordId, @Valid final Url url, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = new ModelAndView("periodRecord/brotherhood/addPicture");
			result.addObject("url", url);
			result.addObject("periodRecordId", periodRecordId);
		} else
			try {
				PeriodRecord periodRecord = this.periodRecordService.findOne(periodRecordId);
				periodRecord.getPictures().add(url);
				periodRecord = this.periodRecordService.save(periodRecord);
				//result = this.createEditModelAndView(c);
				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				System.out.println(url);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = new ModelAndView("periodRecord/brotherhood/addPicture");
				result.addObject("url", url);
				result.addObject("periodRecordId", periodRecordId);
			}
		return result;
	}

	// Ancillary methods -----------------------------------------------------------------------
	protected ModelAndView createEditModelAndView(final PeriodRecord periodRecord) {
		ModelAndView result;

		result = this.createEditModelAndView(periodRecord, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final PeriodRecord periodRecord, final String message) {
		ModelAndView result;

		result = new ModelAndView("periodRecord/brotherhood/edit");
		result.addObject("periodRecord", periodRecord);
		result.addObject("message", message);

		return result;
	}

	private ModelAndView forbiddenOpperation() {
		return new ModelAndView("redirect:/");
	}

	/************************************************************************************************/
}
