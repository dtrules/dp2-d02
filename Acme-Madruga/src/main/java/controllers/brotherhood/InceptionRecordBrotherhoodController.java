
package controllers.brotherhood;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.BrotherhoodService;
import services.InceptionRecordService;
import controllers.AbstractController;
import domain.InceptionRecord;
import domain.Url;

@Controller
@RequestMapping("/inceptionRecord/brotherhood")
public class InceptionRecordBrotherhoodController extends AbstractController {

	@Autowired
	private InceptionRecordService	inceptionRecordService;

	@Autowired
	private BrotherhoodService		brotherhoodService;


	@ExceptionHandler(TypeMismatchException.class)
	public ModelAndView handleMismatchException(final TypeMismatchException oops) {
		return new ModelAndView("redirect:/");
	}

	// Display ------------------------------------------------------------------------------------
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView result;
		InceptionRecord inceptionRecord;

		try {
			inceptionRecord = this.brotherhoodService.findByPrincipal().getHistory().getInceptionRecord();

			result = new ModelAndView("inceptionRecord/brotherhood/display");
			result.addObject("inceptionRecord", inceptionRecord);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	// Create ------------------------------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		InceptionRecord inceptionRecord;

		inceptionRecord = this.inceptionRecordService.create();

		result = this.createEditModelAndView(inceptionRecord);

		return result;
	}

	// Save the inception record ------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final InceptionRecord inceptionRecord, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = this.createEditModelAndView(inceptionRecord);
		} else
			try {
				this.inceptionRecordService.save(inceptionRecord);
				result = new ModelAndView("redirect:display.do");
			} catch (final Throwable oops) {
				System.out.println(inceptionRecord);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.createEditModelAndView(inceptionRecord);

				if (oops instanceof DataIntegrityViolationException)
					result = this.createEditModelAndView(inceptionRecord, "inceptionRecord.commit.username");
				else
					result = this.createEditModelAndView(inceptionRecord, "inceptionRecord.commit.error");
			}
		return result;
	}

	// Edit ------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int inceptionRecordId) {
		ModelAndView result;
		InceptionRecord inceptionRecord;

		try {
			inceptionRecord = this.inceptionRecordService.findOne(inceptionRecordId);
			Assert.notNull(inceptionRecord);
		} catch (final Throwable oops) {
			result = this.forbiddenOpperation();
			return result;
		}

		result = this.createEditModelAndView(inceptionRecord);

		return result;
	}

	// Picture  ------------------------------------------------------------------------------------
	@RequestMapping(value = "/addPicture", method = RequestMethod.GET)
	public ModelAndView addPicture(@RequestParam final int inceptionRecordId) {
		ModelAndView result;
		final Url url;

		try {
			url = new Url();
			result = new ModelAndView("inceptionRecord/brotherhood/addPicture");
			result.addObject("url", url);
			result.addObject("inceptionRecordId", inceptionRecordId);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	@RequestMapping(value = "/deletePicture", method = RequestMethod.GET)
	public ModelAndView deletePicture(@RequestParam final String link, @RequestParam final int inceptionRecordId) {
		ModelAndView result = null;
		try {
			final InceptionRecord inceptionRecord = this.inceptionRecordService.findOne(inceptionRecordId);
			for (final Url picture : inceptionRecord.getPictures())
				if (picture.getLink().equals(link)) {
					inceptionRecord.getPictures().remove(picture);
					this.inceptionRecordService.save(inceptionRecord);
					result = new ModelAndView("redirect:display.do");
					break;
				}
			if (result == null)
				result = this.createEditModelAndView(inceptionRecord);

		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	// SAVE ------------------------------------------------------------------------------------
	@RequestMapping(value = "/addPicture", method = RequestMethod.POST, params = "save")
	public ModelAndView savePicture(@RequestParam final int inceptionRecordId, @Valid final Url url, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = new ModelAndView("inceptionRecord/brotherhood/addPicture");
			result.addObject("url", url);
			result.addObject("inceptionRecordId", inceptionRecordId);
		} else
			try {
				InceptionRecord inceptionRecord = this.inceptionRecordService.findOne(inceptionRecordId);
				inceptionRecord.getPictures().add(url);
				inceptionRecord = this.inceptionRecordService.save(inceptionRecord);
				//result = this.createEditModelAndView(c);
				result = new ModelAndView("redirect:display.do");
			} catch (final Throwable oops) {
				System.out.println(url);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = new ModelAndView("inceptionRecord/brotherhood/addPicture");
				result.addObject("url", url);
				result.addObject("inceptionRecordId", inceptionRecordId);
			}
		return result;
	}

	// Ancillary methods -----------------------------------------------------------------------
	protected ModelAndView createEditModelAndView(final InceptionRecord inceptionRecord) {
		ModelAndView result;

		result = this.createEditModelAndView(inceptionRecord, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final InceptionRecord inceptionRecord, final String message) {
		ModelAndView result;

		result = new ModelAndView("inceptionRecord/brotherhood/edit");
		result.addObject("inceptionRecord", inceptionRecord);
		result.addObject("message", message);

		return result;
	}

	private ModelAndView forbiddenOpperation() {
		return new ModelAndView("redirect:/");
	}

	/************************************************************************************************/
}
