
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import domain.History;

public interface HistoryRepository extends JpaRepository<History, Integer> {

}
