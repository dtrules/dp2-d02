
package repositories;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Administrator;
import domain.Brotherhood;
import domain.Parade;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {

	@Query("select admin from Administrator admin where admin.userAccount.id = ?1")
	Administrator findByUserAccountId(int id);

	//QUERYS ACME MADRUGA

	@Query("select avg(b.enrols.size) from Brotherhood b")
	Double avgMembers();

	@Query("select max(b.enrols.size) from Brotherhood b")
	int maxMembers();

	@Query("select min(b.enrols.size) from Brotherhood b")
	int minMembers();

	@Query("select stddev(b.enrols.size) from Brotherhood b")
	Double stddevMembers();

	@Query("select b1 from Brotherhood b1 where b1.enrols.size = (Select max(b2.enrols.size) from Brotherhood b2)")
	Collection<Brotherhood> largestBrotherhood();

	@Query("select b1 from Brotherhood b1 where b1.enrols.size = (Select min(b2.enrols.size) from Brotherhood b2)")
	Collection<Brotherhood> smallestBrotherhood();

	@Query("select count(r1)*1.0 / (select count(r2)*1.0 from Request r2) from Request r1 group by r1.status")
	Collection<Double> ratioRequests();

	@Query("select p from Parade p where p.moment <= ?1")
	Collection<Parade> paradesOrganised(Date date);

	@Query("select m.name, count(r), (select count(r2)*0.1 from Request r2) from Member m join m.requests r where r.status = 'APPROVED' group by m having count(r) >= (select count(r2)*0.1 from Request r2)")
	Collection<Object> membersLeast10Accepted();

	// Chart queries
	@Query("select count(a) from Actor a where a.isSpammer = true")
	Integer getAllSpammers();

	@Query("select count(a) from Actor a where a.isSpammer = false")
	Integer getAllNotSpammers();

	//ACME PARADE

	@Query("select b from Brotherhood b join b.history h where (1+h.periodRecords.size+h.legalRecords.size+h.linkRecords.size+h.miscellaneousRecords.size) = (select max(1+h2.periodRecords.size+h2.legalRecords.size+h2.linkRecords.size+h2.miscellaneousRecords.size) from Brotherhood b2 join b2.history h2)")
	Collection<Brotherhood> largestHistory();

	@Query("select b from Brotherhood b join b.history h where (1+h.periodRecords.size+h.legalRecords.size+h.linkRecords.size+h.miscellaneousRecords.size) = (select min(1+h2.periodRecords.size+h2.legalRecords.size+h2.linkRecords.size+h2.miscellaneousRecords.size) from Brotherhood b2 join b2.history h2)")
	Collection<Brotherhood> smallestHistory();

	@Query("select b from Brotherhood b join b.history h where (1+h.periodRecords.size+h.legalRecords.size+h.linkRecords.size+h.miscellaneousRecords.size) > (select avg(1+h2.periodRecords.size+h2.legalRecords.size+h2.linkRecords.size+h2.miscellaneousRecords.size) from Brotherhood b2 join b2.history h2)")
	Collection<Brotherhood> largerThanAvg();

	@Query("select avg(1+h.periodRecords.size+h.legalRecords.size+h.linkRecords.size+h.miscellaneousRecords.size) from History h")
	Double avgRecords();

	@Query("select max(1+h.periodRecords.size+h.legalRecords.size+h.linkRecords.size+h.miscellaneousRecords.size) from History h")
	int maxRecords();

	@Query("select min(1+h.periodRecords.size+h.legalRecords.size+h.linkRecords.size+h.miscellaneousRecords.size) from History h")
	int minRecords();

	@Query("select stddev(1+h.periodRecords.size+h.legalRecords.size+h.linkRecords.size+h.miscellaneousRecords.size) from History h")
	Double stddevRecords();
}
