
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import domain.History;
import domain.MiscellaneousRecord;

public interface MiscellaneousRecordRepository extends JpaRepository<MiscellaneousRecord, Integer> {

	@Query("select h from History h join h.miscellaneousRecords mr where mr.id = ?1")
	History findHistoryByMiscellaneousRecordId(int miscellaneousRecordId);

}
