
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import domain.Float;

public interface FloatRepository extends JpaRepository<Float, Integer> {

}
