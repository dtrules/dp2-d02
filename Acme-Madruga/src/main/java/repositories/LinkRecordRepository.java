
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import domain.History;
import domain.LinkRecord;

public interface LinkRecordRepository extends JpaRepository<LinkRecord, Integer> {

	@Query("select h from History h join h.linkRecords lr where lr.id = ?1")
	History findHistoryByLinkRecordId(int linkRecordId);

}
