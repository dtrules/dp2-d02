
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import domain.History;
import domain.LegalRecord;

public interface LegalRecordRepository extends JpaRepository<LegalRecord, Integer> {

	@Query("select h from History h join h.legalRecords lr where lr.id = ?1")
	History findHistoryByLegalRecordId(int legalRecordId);

}
