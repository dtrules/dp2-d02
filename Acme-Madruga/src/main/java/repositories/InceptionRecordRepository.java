
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import domain.History;
import domain.InceptionRecord;

public interface InceptionRecordRepository extends JpaRepository<InceptionRecord, Integer> {

	@Query("select h from History h where h.inceptionRecord.id = ?1")
	History findHistoryByInceptionRecordId(int inceptionRecordId);

}
