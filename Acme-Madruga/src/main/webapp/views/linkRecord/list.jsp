<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<display:table name="linkRecords" id="row" requestURI="linkRecord/brotherhood/list.do" pagesize="5" class="displaytag">

   	<security:authorize access="hasRole('BROTHERHOOD')">
        <display:column>
          <a href="linkRecord/brotherhood/delete.do?linkRecordId=${row.id}">
            <spring:message code="linkRecord.delete"/>
          </a>
        </display:column>
        <display:column>
          <a href="linkRecord/brotherhood/edit.do?linkRecordId=${row.id}">
            <spring:message code="linkRecord.edit"/>
          </a>
        </display:column>
   	</security:authorize>
	
	<!-- Title -->
	<spring:message code="linkRecord.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" />
	
	<!-- Text -->
	<spring:message code="linkRecord.text" var="textHeader" />
	<display:column property="text" title="${textHeader}" />
	
	<!-- URL -->
	<spring:message code="linkRecord.URL" var="URLHeader" />
	<display:column property="URL" title="${URLHeader}" />

</display:table>

<security:authorize access="hasRole('BROTHERHOOD')">
	<a href=linkRecord/brotherhood/create.do><spring:message code="linkRecord.create" /></a>
</security:authorize>