<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="linkRecord/brotherhood/edit.do" modelAttribute="linkRecord">

	<%-- Hidden properties --%>
	<form:hidden path="id" />
	<form:hidden path="version" />

    <%-- title--%>
    <acme:textbox code="linkRecord.title" path="title" />
    <br>

    <%-- text--%>
    <acme:textbox code="linkRecord.text" path="text" />
    <br>
    
    <%-- url--%>
    <acme:textbox code="linkRecord.URL" path="URL" />
    <br>

	<%-- Buttons --%>
	<input type="submit" name="save"
		value="<spring:message code="linkRecord.save"/>"/>
	
	<acme:cancel code="linkRecord.cancel" url="/" />
</form:form>