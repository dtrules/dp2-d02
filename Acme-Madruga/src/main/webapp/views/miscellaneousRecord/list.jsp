<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<display:table name="miscellaneousRecords" id="row" requestURI="miscellaneousRecord/brotherhood/list.do" pagesize="5" class="displaytag">

   <security:authorize access="hasRole('BROTHERHOOD')">
        <display:column>
          <a href="miscellaneousRecord/brotherhood/delete.do?miscellaneousRecordId=${row.id}">
            <spring:message code="miscellaneousRecord.delete"/>
          </a>
        </display:column>
        <display:column>
          <a href="miscellaneousRecord/brotherhood/edit.do?miscellaneousRecordId=${row.id}">
            <spring:message code="miscellaneousRecord.edit"/>
          </a>
        </display:column>
   </security:authorize>
	
	<!-- Title -->
	<spring:message code="miscellaneousRecord.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" />
	
	<!-- Text -->
	<spring:message code="miscellaneousRecord.text" var="textHeader" />
	<display:column property="text" title="${textHeader}" />

</display:table>

<security:authorize access="hasRole('BROTHERHOOD')">
	<a href=miscellaneousRecord/brotherhood/create.do><spring:message code="miscellaneousRecord.create" /></a>
</security:authorize>