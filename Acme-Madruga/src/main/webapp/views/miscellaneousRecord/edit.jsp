<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="miscellaneousRecord/brotherhood/edit.do" modelAttribute="miscellaneousRecord">

	<%-- Hidden properties --%>
	<form:hidden path="id" />
	<form:hidden path="version" />

    <%-- title--%>
    <acme:textbox code="miscellaneousRecord.title" path="title" />
    <br>

    <%-- text--%>
    <acme:textbox code="miscellaneousRecord.text" path="text" />
    <br>

	<%-- Buttons --%>
	<input type="submit" name="save"
		value="<spring:message code="miscellaneousRecord.save"/>"/>
	
	<acme:cancel code="miscellaneousRecord.cancel" url="/" />
</form:form>