<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<spring:message code="administrator.dashboard.avg" 	  var="avgHeader" />
<spring:message code="administrator.dashboard.min" 	  var="minHeader" />
<spring:message code="administrator.dashboard.max" 	  var="maxHeader" />
<spring:message code="administrator.dashboard.std" 	  var="stdHeader" />
<spring:message code="administrator.dashboard.ratio"  var="ratioHeader" />
<spring:message code="administrator.dashboard.name"   var="nameHeader" />
<spring:message code="administrator.dashboard.email"  var="emailHeader" />
<spring:message code="administrator.brotherhood" 	  var="brotherhoodlHeader" />
<spring:message code="administrator.brotherhood.title" 	  var="brotherhoodTitleHeader" />
<spring:message code="administrator.brotherhood.establishment" 	  var="brotherhoodDateHeader" />
<spring:message code="administrator.member" 		  var="memberHeader" />
<spring:message code="administrator.status" 		  var="statusHeader" />
<spring:message code="administrator.procession" 	  var="processionHeader" />
<spring:message code="administrator.moment" 		  var="momentHeader" />
<spring:message code="administrator.position" 		  var="positionHeader" />
<spring:message code="administrator.enrols" 		  var="enrolsHeader" />
<spring:message code="administrator.approved.request" var="aprovedRequestHeader" />
<spring:message code="administrator.total.request"    var="totalRequestHeader" />
<spring:message code="administrator.total.request.bd"    var="totalRequestBDHeader" />



<spring:message code="administrator.dashboard.members.brotherhood" var="membersBrotherhoodHeader"/>
<spring:message code="administrator.dashboard.largest.brotherhood" var="largestBrotherhoodHeader" />
<spring:message code="administrator.dashboard.smallest.brotherhood" var="smallestBrotherhoodHeader" />
<spring:message code="administrator.dashboard.parades" var="paradesHeader" />
<spring:message code="administrator.dashboard.ratios" var="ratiosHeader" />
<spring:message code="administrator.dashboard.members.accepted" var="membersAcceptedHeader" />
<spring:message code="administrator.dashboard.histogram" var="histogramHeader" />
<spring:message code="administrator.dashboard.history.querys" var="historyQuerysHeader" />
<spring:message code="administrator.dashboard.history.records" var="numRecordsHeader" />
<spring:message code="administrator.dashboard.largest.history" var="largestHistoryHeader" />
<spring:message code="administrator.dashboard.larger.avg" var="largerAvgHeader" />
<spring:message code="administrator.dashboard.larger.avg" var="largerAvgHeader" />




<!--  Custom table style -->
<head>
<link rel="stylesheet" href="styles/tablas.css" type="text/css">
<link rel="stylesheet" href="styles/charts.css" type="text/css">
</head>



<!-- Charts -->
<!-- <div class="chart-container"> -->
<!-- 	<div class="pie-chart-container"> -->
<!-- 		<canvas id="pie-chartcanvas-1"></canvas> -->
<!-- 	</div> -->
<!-- </div> -->

<script>
// $(document).ready(function () {
// 	var ctx1 = $("#pie-chartcanvas-1");
	
// 	var data1 = {
// 		labels: ["Spammers", "Not Spammers"],
// 		datasets: [
// 			{
// 				label: "Acme-Madruga",
// 				data: [${spammers}, ${notSpammers}],
// 				backgroundColor: [
// 					"#0000FF",
// 					"#FF0000"
// 				],
// 				borderColor: ["#000000"],
// 				borderWidth: [1]
// 			}
// 		]
// 	};
	
	
// 	var chart1 = new Chart(ctx1, {
// 		type: "pie",
// 		data: data1,
// 		options: {}
// 	});
// });
</script>


<!-- C level -->

<!-- MEMBERS -->
<table>
	<caption>
		<jstl:out value="${membersBrotherhoodHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${avgHeader}"></jstl:out></th>
		<th><jstl:out value="${minHeader}"></jstl:out></th>
		<th><jstl:out value="${maxHeader}"></jstl:out></th>
		<th><jstl:out value="${stdHeader}"></jstl:out></th>
	</tr>

	<tr>
		<td><jstl:out value="${avgMembers}"></jstl:out></td>
		<td><jstl:out value="${maxMembers}"></jstl:out></td>
		<td><jstl:out value="${minMembers}"></jstl:out></td>
		<td><jstl:out value="${stddevMembers}"></jstl:out></td>
	</tr>
</table>
<br />

<!-- Query 2  -->
<table>
	<caption>
		<jstl:out value="${smallestBrotherhoodHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${brotherhoodlHeader}"></jstl:out></th>
		<th><jstl:out value="${memberHeader}"></jstl:out></th>
	</tr>
	<jstl:forEach items="${smallestBrotherhood}" var="row">
      <tr>
        	<td>${row.title}</td>
        	<td>${row.enrols.size()}</td>
      </tr>
   </jstl:forEach>
</table>
<br />

<!-- Query 3  -->
<table>
	<caption>
		<jstl:out value="${largestBrotherhoodHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${brotherhoodlHeader}"></jstl:out></th>
		<th><jstl:out value="${memberHeader}"></jstl:out></th>
	</tr>
	<jstl:forEach items="${largestBrotherhood}" var="row">
      <tr>
        	<td>${row.title}</td>
        	<td>${row.enrols.size()}</td>
      </tr>
   </jstl:forEach>
</table>
<br />

<!-- Query 4  -->
<table>
	<caption>
		<jstl:out value="${ratiosHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${ratioHeader}"></jstl:out></th>
		<th><jstl:out value="${statusHeader}"></jstl:out></th>
	</tr>
	<tr>
		<td><jstl:out value="${ratios[0]}"></jstl:out></td>
		<td><jstl:out value="Aproved"></jstl:out></td>
	</tr>
	<tr>
		<td><jstl:out value="${ratios[1]}"></jstl:out></td>
		<td><jstl:out value="Pending"></jstl:out></td>
	</tr>
	<tr>
		<td><jstl:out value="${ratios[2]}"></jstl:out></td>
		<td><jstl:out value="Rejected"></jstl:out></td>
	</tr>
</table>
<br />

<!-- Query 5  -->
<table>
	<caption>
		<jstl:out value="${paradesHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${processionHeader}"></jstl:out></th>
		<th><jstl:out value="${momentHeader}"></jstl:out></th>
	</tr>
	<jstl:forEach items="${parades}" var="row">
      <tr>
        	<td>${row.title}</td>
        	<td><fmt:formatDate value="${row.moment}" pattern="dd/MM/yyyy" /></td>
      </tr>
   </jstl:forEach>
</table>

<!-- Query 7  -->
<table>
	<caption>
		<jstl:out value="${membersAcceptedHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${memberHeader}"></jstl:out></th>
		<th><jstl:out value="${aprovedRequestHeader}"></jstl:out></th>
		<th><jstl:out value="${totalRequestHeader}"></jstl:out></th>
		<th><jstl:out value="${totalRequestBDHeader}"></jstl:out></th>
	</tr>
	<jstl:forEach items="${membersAccepted}" var="row">
      <tr>
        	<td>${row[0]}</td>
        	<td>${row[1]}</td>
        	<td>${row[2]}</td>
        	<td>${totalRequests}</td>
      </tr>
   </jstl:forEach>
</table>

<!-- Queries ACME Parade -->
<table>
	<caption>
		<jstl:out value="${historyQuerysHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${avgHeader}"></jstl:out></th>
		<th><jstl:out value="${minHeader}"></jstl:out></th>
		<th><jstl:out value="${maxHeader}"></jstl:out></th>
		<th><jstl:out value="${stdHeader}"></jstl:out></th>	
	</tr>
      <tr>
        	<td>${avgRecords}</td>
        	<td>${minRecords}</td>
        	<td>${maxRecords}</td>
        	<td>${stddevRecords}</td>
      </tr>
</table>
<br />

<table>
	<caption>
		<jstl:out value="${largestHistoryHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${brotherhoodTitleHeader}"></jstl:out></th>
		<th><jstl:out value="${brotherhoodDateHeader}"></jstl:out></th>
	</tr>
	<jstl:forEach items="${largestHistory}" var="row">
      <tr>
        	<td>${row.title}</td>
        	<td>${row.establishment}</td>
      </tr>
   </jstl:forEach>
</table>
<br />

<table>
	<caption>
		<jstl:out value="${largerAvgHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${brotherhoodTitleHeader}"></jstl:out></th>
		<th><jstl:out value="${brotherhoodDateHeader}"></jstl:out></th>
		<th><jstl:out value="${avgHeader}"></jstl:out></th>
		<th><jstl:out value="${numRecordsHeader}"></jstl:out></th>
	</tr>
	<jstl:forEach items="${largerHistoryThanAvg}" var="row">
      <tr>
        	<td>${row.title}</td>
        	<td>${row.establishment}</td>
        	<td>${avgRecords}</td>
        	<td>${row.history.periodRecords.size()+row.history.linkRecords.size()+row.history.legalRecords.size()+row.history.miscellaneousRecords.size()+1}</td>
      </tr>
   </jstl:forEach>
</table>
<br />


