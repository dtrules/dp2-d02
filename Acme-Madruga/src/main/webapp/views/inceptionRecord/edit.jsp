<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="inceptionRecord/brotherhood/edit.do" modelAttribute="inceptionRecord">

	<%-- Hidden properties --%>
	<form:hidden path="id" />
	<form:hidden path="version" />


    <%-- title--%>
    <acme:textbox code="inceptionRecord.title" path="title" />
    <br>

    <%-- text--%>
	<acme:textbox code="inceptionRecord.text" path="text" />
    <br>

    <%-- picture --%>
	<jstl:if test="${not empty inceptionRecord.pictures}">
		<jstl:if test="${inceptionRecord.id!=0}">
			<display:table name="inceptionRecord.pictures"  id="row" >
				<spring:message code="inceptionRecord.picture" var="pictureNameHeader" />
				<display:column title="${pictureNameHeader}" sortable="false" >
					<img src="${row.link}" width="50%" height="200"/>
				</display:column>

				<spring:message code="inceptionRecord.pictures.delete" var="deleteHeader" />
				<display:column title="${deleteHeader}">
					<a href="inceptionRecord/brotherhood/deletePicture.do?link=${row.link}&inceptionRecordId=${inceptionRecord.id}"><spring:message code="inceptionRecord.picture.delete"/></a>
				</display:column>

				<display:caption><spring:message code="inceptionRecord.pictures"/></display:caption>
			</display:table>
		</jstl:if>
	</jstl:if>
	<br>
	<jstl:if test="${inceptionRecord.id!=0}">
		<a href="inceptionRecord/brotherhood/addPicture.do?inceptionRecordId=${inceptionRecord.id}">
			<spring:message code="inceptionRecord.picture.create"/>
		</a>
	</jstl:if>
	<br>

	<%-- Buttons --%>
	<input type="submit" name="save"
		value="<spring:message code="inceptionRecord.save"/>"/>
	
	<acme:cancel code="inceptionRecord.cancel" url="/" />
</form:form>