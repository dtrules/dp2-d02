<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="inceptionRecord" id="row" requestURI="inceptionRecord/brotherhood/display.do" class="displaytag">

	<!-- Title -->
	<spring:message code="inceptionRecord.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" />

	<!-- Text -->
	<spring:message code="inceptionRecord.text" var="textHeader" />
	<display:column property="text" title="${textHeader}" />
	
		<spring:message code="inceptionRecord.edit" var="editHeader" />
		<display:column title="${editHeader}">
	<a href="inceptionRecord/brotherhood/edit.do?inceptionRecordId=${row.id}"> <spring:message code="inceptionRecord.edit" /></a>
		</display:column>

</display:table>

<display:table name="inceptionRecord.pictures">

	<!-- Create a header for the sons table -->
	<spring:message code="inceptionRecord.pictures" var="picturesHeader"></spring:message>
	<display:caption style="display: table-caption;text-align: center;">
		<jstl:out value="${picturesHeader}"></jstl:out>
	</display:caption>

	<!-- Make visible the header -->
	<display:setProperty name="basic.show.header" value="true"/>

	<!-- Cell content -->
	<img src="${link}" width="50%" height="200"/>

</display:table>
<br>

<acme:cancel code="inceptionRecord.goback" url="/" />


<br>
<br>