<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<display:table name="parades" id="row" requestURI="${uri}" pagesize="5" class="displaytag">


	<!-- title -->
	<spring:message code="parade.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" />

	<!-- ticker -->
	<spring:message code="parade.ticker" var="tickerHeader" />
	<display:column property="ticker" title="${tickerHeader}" />

	<!-- description -->
	<spring:message code="parade.description" var="descriptionHeader" />
	<display:column property="description" title="${ descriptionHeader }" />

	<!-- moment -->
	<spring:message code="parade.moment" var="momentHeader" />
	<display:column property="moment" title="${momentHeader}" format="{0,date,dd/MM/yyyy}" />

	<!-- brotherhood -->
	<spring:message code="parade.brotherhood" var="brotherhoodHeader" />
	<display:column property="brotherhood.title" title="${brotherhoodHeader}" />
	
	<!-- draftMode -->
<%-- 	<spring:message code="parade.draftMode" var="draftModeHeader" /> --%>
<%-- 	<display:column property="draftMode" title="${draftModeHeader}" /> --%>

	<security:authorize access="hasRole('BROTHERHOOD')">
		<spring:message code="parade.edit" var="editHeader" />
		<display:column title="${editHeader}">
			<jstl:if test="${row.draftMode eq true}">
				<a href="parade/brotherhood/edit.do?paradeId=${row.id}"> <spring:message code="parade.edit" /></a>
			</jstl:if>
			  <jstl:if test="${row.draftMode eq false}">
			<spring:message code="parade.no.draft" />
		 </jstl:if>
		</display:column>
	</security:authorize>

	<security:authorize access="hasRole('BROTHERHOOD')">
		<spring:message code="parade.delete" var="deleteHeader" />
		<display:column title="${deleteHeader}">
		 <jstl:if test="${row.draftMode eq true}">
			<a href="parade/brotherhood/delete.do?paradeId=${row.id}"> <spring:message code="parade.delete" /></a>
		 </jstl:if>
		  <jstl:if test="${row.draftMode eq false}">
			<spring:message code="parade.no.draft" />
		 </jstl:if>
		
		</display:column>
	</security:authorize>

</display:table>

<security:authorize access="hasRole('BROTHERHOOD')">
	<a href=parade/brotherhood/create.do><spring:message code="parade.create" /></a>
</security:authorize>