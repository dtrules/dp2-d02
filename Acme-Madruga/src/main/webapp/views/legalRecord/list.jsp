<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<display:table name="legalRecords" id="row" requestURI="legalRecord/brotherhood/list.do" pagesize="5" class="displaytag">

   	<security:authorize access="hasRole('BROTHERHOOD')">
        <display:column>
          <a href="legalRecord/brotherhood/delete.do?legalRecordId=${row.id}">
            <spring:message code="legalRecord.delete"/>
          </a>
        </display:column>
        <display:column>
          <a href="legalRecord/brotherhood/edit.do?legalRecordId=${row.id}">
            <spring:message code="legalRecord.edit"/>
          </a>
        </display:column>
   	</security:authorize>
	
	<!-- Title -->
	<spring:message code="legalRecord.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" />
	
	<!-- Text -->
	<spring:message code="legalRecord.text" var="textHeader" />
	<display:column property="text" title="${textHeader}" />
	
	<!-- LegalName -->
	<spring:message code="legalRecord.legalName" var="legalNameHeader" />
	<display:column property="legalName" title="${legalNameHeader}" />
	
	<!-- VAT -->
	<spring:message code="legalRecord.VAT" var="VATHeader" />
	<display:column property="VAT" title="${VATHeader}" />
	
	<!-- ApplicableLaws -->
	<spring:message code="legalRecord.applicableLaws" var="applicableLawsHeader" />
	<display:column property="applicableLaws" title="${applicableLawsHeader}" />

</display:table>


<security:authorize access="hasRole('BROTHERHOOD')">
	<a href=legalRecord/brotherhood/create.do><spring:message code="legalRecord.create" /></a>
</security:authorize>