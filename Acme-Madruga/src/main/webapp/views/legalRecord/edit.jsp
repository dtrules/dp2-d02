<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="legalRecord/brotherhood/edit.do" modelAttribute="legalRecord">

	<%-- Hidden properties --%>
	<form:hidden path="id" />
	<form:hidden path="version" />

    <%-- title--%>
    <acme:textbox code="legalRecord.title" path="title" />
    <br>

    <%-- text--%>
    <acme:textbox code="legalRecord.text" path="text" />
    <br>
    
    <%-- legalName--%>
    <acme:textbox code="legalRecord.legalName" path="legalName" />
    <br>
    
    <%-- VAT--%>
    <acme:textbox code="legalRecord.VAT" path="VAT" />
    <br>
    
    <%-- applicableLaws--%>
    <acme:textbox code="legalRecord.applicableLaws" path="applicableLaws" />
    <br>

	<%-- Buttons --%>
	<input type="submit" name="save"
		value="<spring:message code="legalRecord.save"/>"/>
	
	<acme:cancel code="legalRecord.cancel" url="/" />
</form:form>