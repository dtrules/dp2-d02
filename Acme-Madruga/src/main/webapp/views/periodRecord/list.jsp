<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<display:table name="periodRecords" id="row" requestURI="periodRecord/brotherhood/list.do" pagesize="5" class="displaytag">


	
	<!-- Title -->
	<spring:message code="periodRecord.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" />
	
	<!-- Text -->
	<spring:message code="periodRecord.text" var="textHeader" />
	<display:column property="text" title="${textHeader}" />

    <!-- StartYear -->
    <spring:message code="periodRecord.startYear" var="startYearHeader" />
    <display:column property="startYear" title="${startYearHeader}" />

    <!-- EndYear -->
    <spring:message code="periodRecord.endYear" var="endYearHeader" />
    <display:column property="endYear" title="${endYearHeader}" />

	<!-- Picture -->
	<spring:message code="periodRecord.pictures" var="picturesHeader" />
	<display:column property="pictures" title="${picturesHeader}" />

	   <security:authorize access="hasRole('BROTHERHOOD')">
           <display:column>
          <a href="periodRecord/brotherhood/edit.do?periodRecordId=${row.id}">
            <spring:message code="periodRecord.edit"/>
          </a>
        </display:column>
        <display:column>
          <a href="periodRecord/brotherhood/delete.do?periodRecordId=${row.id}">
            <spring:message code="periodRecord.delete"/>
          </a>
        </display:column>
   </security:authorize>
   
</display:table>

<security:authorize access="hasRole('BROTHERHOOD')">
	<a href=periodRecord/brotherhood/create.do><spring:message code="periodRecord.create" /></a>
</security:authorize>