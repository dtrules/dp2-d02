<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="periodRecord/brotherhood/edit.do" modelAttribute="periodRecord">

	<%-- Hidden properties --%>
	<form:hidden path="id" />
	<form:hidden path="version" />


    <%-- title--%>
    <acme:textbox code="periodRecord.title" path="title" />
    <br>

    <%-- text--%>
    <acme:textbox code="periodRecord.text" path="text" />
    <br>

	<%-- startYear--%>
	<acme:textbox code="periodRecord.startYear" path="startYear" />
	<br>

	<%-- endYear--%>
	<acme:textbox code="periodRecord.endYear" path="endYear" />
	<br>

    <%-- picture --%>
	<jstl:if test="${not empty periodRecord.pictures}">
		<jstl:if test="${periodRecord.id!=0}">
			<display:table name="periodRecord.pictures"  id="row" >
				<spring:message code="periodRecord.picture" var="pictureNameHeader" />
				<display:column title="${pictureNameHeader}" sortable="false" >
					<img src="${row.link}" width="50%" height="200"/>
				</display:column>

				<spring:message code="periodRecord.pictures.delete" var="deleteHeader" />
				<display:column title="${deleteHeader}">
					<a href="periodRecord/brotherhood/deletePicture.do?link=${row.link}&periodRecordId=${periodRecord.id}"><spring:message code="periodRecord.picture.delete"/></a>
				</display:column>

				<display:caption><spring:message code="periodRecord.pictures"/></display:caption>
			</display:table>
		</jstl:if>
	</jstl:if>
	<br>
	<jstl:if test="${periodRecord.id!=0}">
		<a href="periodRecord/brotherhood/addPicture.do?periodRecordId=${periodRecord.id}">
			<spring:message code="periodRecord.picture.create"/>
		</a>
	</jstl:if>
	<br>

	<%-- Buttons --%>
	<input type="submit" name="save"
		value="<spring:message code="periodRecord.save"/>"/>
	
	<acme:cancel code="periodRecord.cancel" url="/" />
</form:form>