<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<h2> <spring:message code="brotherhood.inceptionRecord"/> </h2>

<display:table name="inceptionRecords" id="row" requestURI="brotherhood/history.do" pagesize="5" class="displaytag">

	<!-- Title -->
	<spring:message code="brotherhood.record.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" />.

    <!-- Text -->
    <spring:message code="brotherhood.record.text" var="textHeader" />
    <display:column property="text" title="${textHeader}" />

    <!-- Pictures -->
    <spring:message code="brotherhood.record.pictures" var="picturesHeader" />
    <display:column property="pictures" title="${picturesHeader}" />

</display:table>

<h2> <spring:message code="brotherhood.periodRecord"/> </h2>

<display:table name="periodRecords" id="row" requestURI="brotherhood/history.do" pagesize="5" class="displaytag">

    <!-- Title -->
    <spring:message code="brotherhood.record.title" var="titleHeader" />
    <display:column property="title" title="${titleHeader}" />.

    <!-- Text -->
    <spring:message code="brotherhood.record.text" var="textHeader" />
    <display:column property="text" title="${textHeader}" />

    <!-- Start Year -->
    <spring:message code="brotherhood.record.startYear" var="startYearHeader" />
    <display:column property="startYear" title="${startYearHeader}" />

    <!-- End Year -->
    <spring:message code="brotherhood.record.endYear" var="endYearHeader" />
    <display:column property="endYear" title="${endYearHeader}" />

    <!-- Pictures -->
    <spring:message code="brotherhood.record.pictures" var="picturesHeader" />
    <display:column property="pictures" title="${picturesHeader}" />

</display:table>

<h2> <spring:message code="brotherhood.legalRecord"/> </h2>

<display:table name="legalRecords" id="row" requestURI="brotherhood/history.do" pagesize="5" class="displaytag">

    <!-- Title -->
    <spring:message code="brotherhood.record.title" var="titleHeader" />
    <display:column property="title" title="${titleHeader}" />.

    <!-- Text -->
    <spring:message code="brotherhood.record.text" var="textHeader" />
    <display:column property="text" title="${textHeader}" />
    
    <!-- LegalName -->
    <spring:message code="brotherhood.record.legalName" var="textHeader" />
    <display:column property="legalName" title="${legalNameHeader}" />

    <!-- VAT -->
    <spring:message code="brotherhood.record.VAT" var="VATHeader" />
    <display:column property="VAT" title="${VATHeader}" />

    <!-- ApplicableLaws -->
    <spring:message code="brotherhood.record.applicableLaws" var="applicableLawsHeader" />
    <display:column property="applicableLaws" title="${applicableLawsHeader}" />

</display:table>

<h2> <spring:message code="brotherhood.linkRecord"/> </h2>

<display:table name="linkRecords" id="row" requestURI="brotherhood/history.do" pagesize="5" class="displaytag">

    <!-- Title -->
    <spring:message code="brotherhood.record.title" var="titleHeader" />
    <display:column property="title" title="${titleHeader}" />.

    <!-- Text -->
    <spring:message code="brotherhood.record.text" var="textHeader" />
    <display:column property="text" title="${textHeader}" />

    <!-- URL -->
    <spring:message code="brotherhood.record.URL" var="URLHeader" />
    <display:column property="URL" title="${URLHeader}" />

</display:table>


<h2> <spring:message code="brotherhood.miscellaneousRecord"/> </h2>

<display:table name="miscellaneousRecords" id="row" requestURI="brotherhood/history.do" pagesize="5" class="displaytag">

    <!-- Title -->
    <spring:message code="brotherhood.record.title" var="titleHeader" />
    <display:column property="title" title="${titleHeader}" />.

    <!-- Text -->
    <spring:message code="brotherhood.record.text" var="textHeader" />
    <display:column property="text" title="${textHeader}" />

</display:table>
