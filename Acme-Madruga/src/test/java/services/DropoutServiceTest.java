
package services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import utilities.AbstractTest;
import domain.Dropout;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class DropoutServiceTest extends AbstractTest {

	//SUT -----------------------------------------------------------

	@Autowired
	private DropoutService		dropoutService;

	@Autowired
	private BrotherhoodService	brotherhoodService;

	@Autowired
	private MemberService		memberService;


	// Tests de requisitos 11. An actor who is authenticated as a member must be able to:
	//		2. Drop out from a brotherhood to which he or she belongs. 
	//		The system must record the moment then the drop out takes place. 
	//		A member may be re-enrolled after he or she drops out.

	// test positivo de crear un dropout de un miembro
	@Test
	public void testDropoutFromBrotherhood() {
		int brotherhoodId;

		brotherhoodId = super.getEntityId("brotherhood1");
		super.authenticate("member1");
		final Dropout dropout = this.dropoutService.create();

		dropout.setBrotherhood(this.brotherhoodService.findOne(brotherhoodId));
		dropout.setMember(this.memberService.findByPrincipal());

		super.unauthenticate();

	}
	// test negativo intentar crear un dropout con un brotherhood, no se debe de poder guardar un dropout de una brotherhood a una brotherhood
	@Test(expected = IllegalArgumentException.class)
	public void testDropoutFromBrotherhoodAsBrotherhood() {

		int brotherhoodId;

		brotherhoodId = super.getEntityId("brotherhood1");
		super.authenticate("brotherhood2");
		final Dropout dropout = this.dropoutService.create();

		dropout.setBrotherhood(this.brotherhoodService.findOne(brotherhoodId));
		dropout.setMember(this.memberService.findByPrincipal());

		super.unauthenticate();

	}

}
