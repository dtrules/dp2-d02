
package services;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.LegalRecord;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class LegalRecordServiceTest extends AbstractTest {

	//Service under test ---------------------------

	//D) data coverage 77.6%

	@Autowired
	private LegalRecordService	legalRecordService;

	@Autowired
	private BrotherhoodService	brotherhoodService;


	//Caso de uso en el que queremos crear nuestro legal record

	protected void createLegalRecordTemplate(final String username, final String title, final String text, final String legalName, final int VAT, final String applicableLaws, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			final LegalRecord lr = this.legalRecordService.create();

			lr.setTitle(title);
			lr.setText(text);
			lr.setLegalName(legalName);
			lr.setVAT(VAT);
			lr.setApplicableLaws(applicableLaws);

			this.legalRecordService.save(lr);
			this.legalRecordService.flush();

			super.unauthenticate();

		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	//Caso de uso en el que queremos editar nuestro legal record

	protected void editLegalRecordTemplate(final String username, final String title, final String text, final String legalName, final int VAT, final String applicableLaws, final int legalRecordId, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			final LegalRecord lr = this.legalRecordService.findOne(legalRecordId);

			lr.setTitle(title);
			lr.setText(text);
			lr.setLegalName(legalName);
			lr.setVAT(VAT);
			lr.setApplicableLaws(applicableLaws);

			this.legalRecordService.save(lr);
			this.legalRecordService.flush();

			super.unauthenticate();

		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	//Caso en el que queremos listar los legal records
	protected void listLegalRecordTemplate(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.legalRecordService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}

	}

	//Caso en el que queremos borrar una posicion
	public void deleteLegalRecordTemplate(final String username, final int legalRecordId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Buscamos el miscellaneousRecord
			final LegalRecord lr = this.legalRecordService.findOne(legalRecordId);

			// Borramos
			this.legalRecordService.delete(legalRecordId);

			// Comprobamos que se ha borrado
			//Assert.isNull(this.positionService.findOne(posId));

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateLegalRecord() {
		// c) sentence coverage 100%
		final Object testingData[][] = {

			// Crear legal record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Titletest1", "Texttest1", "Legal1", 21, "Laws1", null
			},
			// Crear legal record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", "Legal1", 21, "Laws1", null
			},
			// Crear legal record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "asdasdad", "sfsafdsfsafs", "Legal1", 21, "Laws1", null
			},
			// Crear legal record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", "Legal1", 21, "Laws1", null
			},
			// Crear legal record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Title3333", "sfsafdsfsafs", "Legal1", 21, "Laws1", null
			},
			// Crear legal record con datos invalidos -> false
			{
				"brotherhood1", "", "", "", 4, "", ConstraintViolationException.class
			},
			// Crear legal record con datos invalidos VAT nulo -> false
			{
				"brotherhood2", "weffew", "asdad", "", 6, "Laws1", ConstraintViolationException.class
			},
			// Crear legal record con otro usuario -> false
			{
				"member1", "Title3333", "sfsafdsfsafs", "Legal1", 21, "Laws1", IllegalArgumentException.class
			},
			// Crear legal record con otro usuario -> false
			{
				"admin", "Title3333", "sfsafdsfsafs", "Legal1", 21, "Laws1", IllegalArgumentException.class
			},
			// Crear legal record con otro usuario -> false
			{
				"member2", "Title3333", "sfsafdsfsafs", "Legal1", 21, "Laws1", IllegalArgumentException.class
			},
		};

		System.out.println("---CREATE----");

		for (int i = 0; i < testingData.length; i++)
			this.createLegalRecordTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (int) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	@Test
	public void driverEditLegalRecord() {

		final Object testingData[][] = {

			// Editar legal record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Titletest1", "Texttest1", "Legal1", 21, "Laws1", 1485, null
			},
			// Editar legal record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", "Legal1", 21, "Laws1", 1486, null
			},
			// Editar legal record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "asdasdad", "sfsafdsfsafs", "Legal1", 21, "Laws1", 1485, null
			},
			// Editar legal record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", "Legal1", 21, "Laws1", 1486, null
			},
			// Editar legal record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Title3333", "sfsafdsfsafs", "Legal1", 21, "Laws1", 1485, null
			},
			// Editar legal record con datos invalidos -> false
			{
				"brotherhood1", "", "", "", 5, "", 1485, ConstraintViolationException.class
			},
			// Editar legal record con datos invalidos -> false
			{
				"brotherhood2", "asdad", "asdad", "", 6, "Laws1", 1486, ConstraintViolationException.class
			},
			// Editar legal record con otro usuario -> false
			{
				"member1", "Title3333", "sfsafdsfsafs", "Legal1", 21, "Laws1", 23, IllegalArgumentException.class
			},
			// Editar legal record con otro usuario -> false
			{
				"admin", "Title3333", "sfsafdsfsafs", "Legal1", 21, "Laws1", 23, IllegalArgumentException.class
			},
			// Editar legal record con otro usuario -> false
			{
				"member2", "Title3333", "sfsafdsfsafs", "Legal1", 21, "Laws1", 23, IllegalArgumentException.class
			},
			// Editar el legal record de otro usuario -> false
			{
				"brotherhood2", "asdad", "asdad", "Legal1", 21, "Laws1", 1485, IllegalArgumentException.class
			},
		};

		System.out.println("---EDIT----");

		for (int i = 0; i < testingData.length; i++)
			this.editLegalRecordTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (int) testingData[i][4], (String) testingData[i][5], (int) testingData[i][6],
				(Class<?>) testingData[i][7]);
	}

	@Test
	public void driverListLegalRecord() {
		final Object testingData[][] = {
			// Se accede con brotherhood -> true
			{
				"brotherhood1", null
			},
			// Se accede con member -> true
			{
				"member1", null
			},
			//  Se accede con admin -> true
			{
				"admin", null
			},

			//  Se accede con usuario no autentificado -> true
			{
				null, null
			}
		};
		System.out.println("---LIST----");
		for (int i = 0; i < testingData.length; i++)
			this.listLegalRecordTemplate((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverDeleteLegalRecord() {
		final Object testingData[][] = {

			//c) sentence coverage 0%

			// Se accede con brotherhood y datos validos -> true
			{
				"brotherhood1", 1485, null
			},
			// Se accede con brotherhood y datos validos -> true
			{
				"brotherhood2", 1486, null
			},
			//  Se accede con otro actor logueado -> false
			{
				"member1", 386, IllegalArgumentException.class
			},
			//  Se accede con otro actor logueado -> false
			{
				"admin", 388, IllegalArgumentException.class
			},
			// Se da una id de un legal record de otro brotherhood
			{
				"brotherhood2", 1485, IllegalArgumentException.class
			},
		};
		System.out.println("---DELETE----");
		for (int i = 0; i < testingData.length; i++)
			this.deleteLegalRecordTemplate((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
