
package services;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.MiscellaneousRecord;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class MiscellaneousRecordServiceTest extends AbstractTest {

	// D) data covereage 60%

	//Service under test ---------------------------

	@Autowired
	private MiscellaneousRecordService	miscellaneousRecordService;

	@Autowired
	private BrotherhoodService			brotherhoodService;


	//Caso de uso en el que queremos crear nuestro miscellaneous record

	protected void createMiscellaneousRecordTemplate(final String username, final String title, final String text, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			final MiscellaneousRecord mr = this.miscellaneousRecordService.create();

			mr.setTitle(title);
			mr.setText(text);

			this.miscellaneousRecordService.save(mr);
			this.miscellaneousRecordService.flush();

			super.unauthenticate();

		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	//Caso de uso en el que queremos editar nuestro miscellaneous record

	protected void editMiscellaneousRecordTemplate(final String username, final String title, final String text, final int miscellaneousRecordId, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			final MiscellaneousRecord mr = this.miscellaneousRecordService.findOne(miscellaneousRecordId);

			mr.setTitle(title);
			mr.setText(text);

			this.miscellaneousRecordService.save(mr);
			this.miscellaneousRecordService.flush();

			super.unauthenticate();

		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	//Caso en el que queremos listar los miscellaneous records
	protected void listMiscellaneousRecordTemplate(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.miscellaneousRecordService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}

	}

	//Caso en el que queremos borrar una posicion
	public void deleteMiscellaneousRecordTemplate(final String username, final int miscellaneousRecordId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Buscamos el miscellaneousRecord
			final MiscellaneousRecord mr = this.miscellaneousRecordService.findOne(miscellaneousRecordId);

			// Borramos
			this.miscellaneousRecordService.delete(miscellaneousRecordId);

			// Comprobamos que se ha borrado
			//Assert.isNull(this.positionService.findOne(posId));

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateMiscellaneousRecord() {

		// c) sentence coverage 100%
		final Object testingData[][] = {

			// Crear miscellaneous record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Titletest1", "Texttest1", null
			},
			// Crear miscellaneous record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", null
			},
			// Crear miscellaneous record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "asdasdad", "sfsafdsfsafs", null
			},
			// Crear miscellaneous record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", null
			},
			// Crear miscellaneous record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Title3333", "sfsafdsfsafs", null
			},
			// Crear miscellaneous record con datos invalidos -> false
			{
				"brotherhood1", "", "", ConstraintViolationException.class
			},
			// Crear miscellaneous record con datos invalidos -> false
			{
				"brotherhood2", "asdad", "", ConstraintViolationException.class
			},
			// Crear miscellaneous record con otro usuario -> false
			{
				"member1", "Title3333", "sfsafdsfsafs", IllegalArgumentException.class
			},
			// Crear miscellaneous record con otro usuario -> false
			{
				"admin", "Title3333", "sfsafdsfsafs", IllegalArgumentException.class
			},
			// Crear miscellaneous record con otro usuario -> false
			{
				"member2", "Title3333", "sfsafdsfsafs", IllegalArgumentException.class
			},
		};

		System.out.println("---CREATE----");

		for (int i = 0; i < testingData.length; i++)
			this.createMiscellaneousRecordTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void driverEditMiscellaneousRecord() {

		final Object testingData[][] = {

			// Editar miscellaneous record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Titletest1", "Texttest1", 1489, null
			},
			// Editar miscellaneous record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", 1490, null
			},
			// Editar miscellaneous record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "asdasdad", "sfsafdsfsafs", 1489, null
			},
			// Editar miscellaneous record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", 1490, null
			},
			// Editar miscellaneous record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Title3333", "sfsafdsfsafs", 1489, null
			},
			// Editar miscellaneous record con datos invalidos -> false
			{
				"brotherhood1", "", "", 23, IllegalArgumentException.class
			},
			// Editar miscellaneous record con datos invalidos -> false
			{
				"brotherhood2", "asdad", "asdad", 35, IllegalArgumentException.class
			},
			// Editar miscellaneous record con otro usuario -> false
			{
				"member1", "Title3333", "sfsafdsfsafs", 23, IllegalArgumentException.class
			},
			// Editar miscellaneous record con otro usuario -> false
			{
				"admin", "Title3333", "sfsafdsfsafs", 23, IllegalArgumentException.class
			},
			// Editar miscellaneous record con otro usuario -> false
			{
				"member2", "Title3333", "sfsafdsfsafs", 23, IllegalArgumentException.class
			},
			// Editar miscellaneous record con otro usuario -> false
			{
				"brotherhood2", "asdad", "asdad", 23, IllegalArgumentException.class
			},
		};

		System.out.println("---EDIT----");

		for (int i = 0; i < testingData.length; i++)
			this.editMiscellaneousRecordTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (int) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void driverListMiscellaneousRecord() {
		final Object testingData[][] = {
			// Se accede con brotherhood -> true
			{
				"brotherhood1", null
			},
			// Se accede con member -> true
			{
				"member1", null
			},
			//  Se accede con admin -> true
			{
				"admin", null
			},

			//  Se accede con usuario no autentificado -> true
			{
				null, null
			}
		};
		System.out.println("---LIST----");
		for (int i = 0; i < testingData.length; i++)
			this.listMiscellaneousRecordTemplate((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverDeleteMiscellaneousRecords() {
		final Object testingData[][] = {

			// c) sentence coverage 0%
			// Se accede con brotherhood y datos validos -> true
			{
				"brotherhood1", 1489, null
			},
			// Se accede con brotherhood y una id de misc que le pertenece -> true
			{
				"brotherhood2", 1490, null
			},
			//  Se accede con otro actor logueado -> false
			{
				"member1", 386, IllegalArgumentException.class
			},
			//  Se accede con otro actor logueado -> false
			{
				"admin", 388, IllegalArgumentException.class
			},
			// Se da una id de un miscellaneous record de otro brotherhood
			{
				"brotherhood2", 1490, IllegalArgumentException.class
			},
		};
		System.out.println("---DELETE----");
		for (int i = 0; i < testingData.length; i++)
			this.deleteMiscellaneousRecordTemplate((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
