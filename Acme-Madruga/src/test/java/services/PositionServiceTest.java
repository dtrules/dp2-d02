
package services;

import java.util.Collection;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Enrol;
import domain.Position;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class PositionServiceTest extends AbstractTest {

	//Service under test ---------------------------

	// D: 100% data coverage
	@Autowired
	private PositionService	positionService;

	@Autowired
	private EnrolService	enrolService;


	//Caso de uso en el que creamos una posicion
	protected void createPositionTemplate(final String username, final String spName, final String engName, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			final Position pos = this.positionService.create();
			final Collection<Enrol> enrols = this.enrolService.findAll();

			pos.setSpanishName(spName);
			pos.setEnglishName(engName);
			pos.setEnrol(enrols);

			this.positionService.save(pos);
			this.positionService.flush();

			super.unauthenticate();

		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	//Caso de uso en el que actualizamos una posicion
	protected void editPositionTemplate(final String username, final int id, final String espName, final String engName, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			final Position pos = this.positionService.findOne(id);

			pos.setEnglishName(engName);
			pos.setSpanishName(espName);

			this.positionService.save(pos);
			this.positionService.flush();

			super.unauthenticate();

		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	//Caso de uso en el que se muestra una posicion
	protected void showPositionTemplate(final String username, final int posId, final Class<?> expected) {

		Class<?> caught = null;

		try {
			this.positionService.findOne(posId);

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}

	}

	//Caso en el que queremos listar las posiciones
	protected void listPositionTemplate(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.positionService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}

	}

	//Caso en el que queremos borrar una posicion
	public void deletePositionTemplate(final String username, final int posId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Buscamos la posicion
			final Position pos = this.positionService.findOne(posId);

			// Borramos
			this.positionService.delete(pos);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreatePosition() {
		//A: Req 12.2 Creating positions

		//C: 100% sentence coverage

		final Object testingData[][] = {

			//A: Test positivo

			{
				"admin", "posicion1", "position1", null
			},
			//B: Test negativo ---> intentar crear un posicion logueado con otro actor

			{
				"member1", "posicion2", "position2", IllegalArgumentException.class
			},
			//B: Test negativo --> intentar crear una posicion con los campos en blanco @NotBlank

			{
				"admin", "", "", ConstraintViolationException.class
			},

		};

		System.out.println("---CREATE----");

		for (int i = 0; i < testingData.length; i++)
			this.createPositionTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void driverEditPosition() {
		//A: Req 12.2 Editing positions
		final Object testingData[][] = {

			//A: Test positivo

			{
				"admin", 1471, "spname1", "engname1", null
			},
			//B: Test negativo --> intentar editar una position logueado con otro actor

			{
				"member1", 1471, "spname2", "engname2", IllegalArgumentException.class
			},
			//B: Test negativo --> pasarle una id no existente

			{
				"admin", 39770, "spname2", "engname2", IllegalArgumentException.class
			},

		};

		System.out.println("---EDIT----");

		for (int i = 0; i < testingData.length; i++)
			this.editPositionTemplate((String) testingData[i][0], (int) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void driverShowPosition() {
		//A: Req 12.2 Showing positions
		final Object testingData[][] = {
			//B: Test positivo

			{
				"admin", 1471, null
			},

			//B: Test negativo ---> buscar una posicion con una id no existente

			{
				"member1", 390, IllegalArgumentException.class
			},
		};

		System.out.println("---SHOW----");

		for (int i = 0; i < testingData.length; i++)
			this.showPositionTemplate((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void driverListPosition() {
		//A: Req 12.2 Listing positions
		final Object testingData[][] = {
			//B: Test positivo

			{
				"brotherhood1", null
			},
			//A: Test positivo

			{
				"member1", null
			},
			//A: Test positivo

			{
				"admin", null
			},

			//A: Test positivo

			{
				null, null
			}
		};
		System.out.println("---LIST----");
		for (int i = 0; i < testingData.length; i++)
			this.listPositionTemplate((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverDeletePosition() {
		//A: Req 12.2 Deleting positions
		//C: 96% sentence coverage
		final Object testingData[][] = {
			//A: Test positivo

			{
				"admin", 1472, null
			},
			//B: Test negativo --> se intenta borrar una position logueado con otro actor

			{
				"member1", 1471, IllegalArgumentException.class
			},
			//B: Test negativo --> se intenta borrar una position con una id inexistente

			{
				"admin", 390, IllegalArgumentException.class
			},
			//B: Test negativo --> se intenta borrar una position que tiene enrols

			{
				"admin", 1471, IllegalArgumentException.class
			},
		};
		System.out.println("---DELETE----");
		for (int i = 0; i < testingData.length; i++)
			this.deletePositionTemplate((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
