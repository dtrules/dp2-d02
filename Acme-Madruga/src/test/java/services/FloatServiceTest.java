
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import utilities.AbstractTest;
import domain.Url;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class FloatServiceTest extends AbstractTest {

	//Requisito 10.1/10.4 tests a hacer, crear un float, actualizar un float, borrar un float

	//D) data coverage 100%
	@Autowired
	private FloatService	floatService;


	@Test
	public void driverCreate() {

		//sentence coverage 100%

		final Collection<Url> urls = new ArrayList<Url>();

		final Object testingData[][] = {

			//a)test positivo de crear un float
			{
				"brotherhood1", "TITLE", "DESCRIPTION", urls, "test positivo", null
			},
			//a) test negativo de crear un float sin titulo
			//b) el titulo de un float no debe ser null

			{
				"brotherhood1", null, "DESCRIPTION", null, "intentar guardar un float sin titulo", ConstraintViolationException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Collection<Url>) testingData[i][3], (String) testingData[i][4], (Class<?>) testingData[i][5]);
	}
	protected void templateCreate(final String username, final String title, final String description, final Collection<Url> photos, final String explanation, final Class<?> expected) {
		Class<?> caught = null;

		try {
			this.authenticate(username);
			final domain.Float carroza = this.floatService.create();
			carroza.setTitle(title);
			carroza.setDescription(description);
			carroza.setPictures(photos);

			this.floatService.save(carroza);
			this.floatService.flush();

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		super.checkExceptions(expected, caught);

	}

	@Test
	public void driverUpdate() {
		final Object testingData[][] = {
			//test positivo de actualizar el titulo de un float
			{
				"float1", "titulo actualizado", null
			},
			//test negativo actualizando un float con un titulo null
			{
				"float1", "", ConstraintViolationException.class
			}

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateUpdate((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);

	}
	protected void templateUpdate(final String paso, final String titulo, final Class<?> expected) {
		Class<?> caught;

		caught = null;

		try {
			this.authenticate("brotherhood1");
			final domain.Float carroza = this.floatService.findOne(super.getEntityId(paso));

			carroza.setTitle(titulo);

			this.floatService.save(carroza);
			this.floatService.flush();

			super.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		super.checkExceptions(expected, caught);

	}

	// Tests del delete
	@Test
	public void driverDelete() {

		// sentence coverate 97.3 %
		final Object testingData[][] = {
			//test positivo de borrar un loat que existe

			{
				"float1", null
			}
			//Test negativo de borrar un float que no existe
			, {
				"float99", AssertionError.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateDelete((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void templateDelete(final String paso, final Class<?> expected) {

		Class<?> caught = null;

		try {
			this.authenticate("brotherhood1");

			this.floatService.delete(super.getEntityId(paso));
		} catch (final Throwable oops) {
			caught = oops.getClass();

		}
		super.checkExceptions(expected, caught);
	}

}
