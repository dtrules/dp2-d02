/*
 * AdministratorServiceTest.java
 * 
 * Copyright (C) 2019 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import security.UserAccount;
import utilities.AbstractTest;
import domain.Administrator;
import domain.Brotherhood;
import domain.Parade;

@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class AdministratorServiceTest extends AbstractTest {

	// System under test ------------------------------------------------------

	// D) Data coverage 62.5

	@Autowired
	private AdministratorService	administratorService;

	@Autowired
	private ActorService			actorService;


	//Supporting services -----------------------------------------------------

	// Tests ------------------------------------------------------------------

	// The following are fictitious test cases that are intended to check that 
	// JUnit works well in this project.  Just righ-click this class and run 
	// it using JUnit.

	public void createAdmin(final String username, final String newUsername, final String password, final String name, final String middleName, final String surname, final String photo, final String email, final String phoneNumber, final String address,
		final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Inicializamos los atributos para la creaci�n
			Administrator administrator = this.administratorService.create();

			administrator.setName(name);
			administrator.setMiddleName(middleName);
			administrator.setSurname(surname);
			administrator.setPhoto(photo);
			administrator.setEmail(email);
			administrator.setPhoneNumber(phoneNumber);
			administrator.setAddress(address);

			final UserAccount ua = administrator.getUserAccount();
			ua.setUsername(newUsername);
			ua.setPassword(password);
			administrator.setUserAccount(ua);

			// Guardamos
			administrator = this.administratorService.save(administrator);

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverCreateAdmin() {

		// sentence coverage 100%

		final Object testingData[][] = {

			// a) Registrar admin estando autentificado -> true
			{
				"admin", "admin223", "administrator2", "Admin2Name", "Admin2middlename", "Admin2surname", "http://www.linkedin.com/", "612239922", "admin2@gmail.com", "Abbey Road, 21", null
			},
			// a) Registrar admin sin estar logueado --> false
			// b) Un usuario que esta logueado como administrador debe ser capaz de crear cuentas de usuario para nuevos administradores
			{
				null, "admin2", "admin21", "Admin2Name", "Admin2middlename", "Admin2surname", "http://www.linkedin.com", "612239922", "admin2@gmail.com", "Abbey Road, 21", IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.createAdmin((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (String) testingData[i][9], (Class<?>) testingData[i][10]);
	}

	//TEST DASHBOARD

	protected void testLargestBrotherhood(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST LARGEST BROTHERHOOD --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Collection<Brotherhood> brotherhood = this.administratorService.largestBrotherhood();

			final String print = brotherhood.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverLargestBrotherhood() {

		final Object testingData[][] = {

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testLargestBrotherhood((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testSmallestBrotherhood(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST SMALLEST BROTHERHOOD --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Collection<Brotherhood> brotherhood = this.administratorService.smallestBrotherhood();
			final String print = brotherhood.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverSmallestBrotherhood() {

		final Object testingData[][] = {

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testSmallestBrotherhood((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testRequestsRatio(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST REQUESTS RATIO --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Collection<Double> ratios = this.administratorService.ratioRequests();
			final String print = ratios.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverRequestRatio() {

		final Object testingData[][] = {

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testRequestsRatio((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testAvgMembers(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST AVG MEMBERS --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Double avgMembers = this.administratorService.avgMembers();
			final String print = avgMembers.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverAvgMembers() {

		final Object testingData[][] = {

			// sentence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testAvgMembers((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testMinMembers(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST MIN MEMBERS --------------------------------------------------");
			super.authenticate(username);

			@SuppressWarnings("unused")
			final int minMembers = this.administratorService.minMembers();
			System.out.println(minMembers);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverMinMembers() {

		final Object testingData[][] = {

			// setence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testMinMembers((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testMaxMembers(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST MAX MEMBERS --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final int maxMembers = this.administratorService.maxMembers();
			System.out.print(maxMembers);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverMaxMembers() {

		// setence coverage 100%

		final Object testingData[][] = {

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testMaxMembers((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testStddevMembers(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST STDDEV MEMBERS --------------------------------------------------");
			super.authenticate(username);

			@SuppressWarnings("unused")
			final Double stddevMembers = this.administratorService.stddevMembers();
			System.out.print(stddevMembers);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverStddevMembers() {

		// setence coverage 100%

		final Object testingData[][] = {

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testStddevMembers((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testLargestHistory(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST LARGEST HISTORY --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Collection<Brotherhood> largHistory = this.administratorService.largestHistory();

			final String print = largHistory.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverLargestHistory() {

		// setence coverage 100%

		final Object testingData[][] = {

			// a) Mostrar información de la dashboard como admin

			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testLargestHistory((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testSmallestHistory(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST SMALLEST HISTORY --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Collection<Brotherhood> smallHistory = this.administratorService.smallestHistory();
			final String print = smallHistory.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverSmallestHistory() {

		// setence coverage 100%

		final Object testingData[][] = {

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testSmallestHistory((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testLargerThanAvg(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST LARGER THAN AVG --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Collection<Brotherhood> largerThanAvg = this.administratorService.largerThanAvg();
			final String print = largerThanAvg.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverLargerThanAvg() {

		// setence coverage 100%

		final Object testingData[][] = {

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard

			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testLargerThanAvg((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testParadesOrganised(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {
			System.out.println("------------------------------------TEST PARADES ORGANISED --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Collection<Parade> parades = this.administratorService.paradesOrganised();
			final String print = parades.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverParadesOrganised() {

		// setence coverage 100%

		final Object testingData[][] = {

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testParadesOrganised((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
	protected void testMembersAccepted(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {
			System.out.println("------------------------------------TEST MEMBERS ACCEPTED --------------------------------------------------");
			super.authenticate(username);

			@SuppressWarnings("unused")
			final Collection<Object> members = this.administratorService.membersLeast10Accepted();
			final String print = members.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverMembersAccepted() {

		// setence coverage 100%

		final Object testingData[][] = {

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard			
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testMembersAccepted((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
	protected void testAvgRecords(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST AVG RECORDS --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Double avgRec = this.administratorService.avgRecords();
			final String print = avgRec.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverAvgRecords() {

		// setence coverage 100%

		final Object testingData[][] = {

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard						
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testAvgRecords((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testMaxRecords(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST MAX RECORDS --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final int maxRec = this.administratorService.maxRecords();
			System.out.println(maxRec);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}
	@Test
	public void driverMaxRecords() {

		// setence coverage 100%

		final Object testingData[][] = {

			// a) Mostrar información de la dashboard como admin

			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard	
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testMaxRecords((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testMinRecords(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST MIN RECORDS --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final int minRec = this.administratorService.minRecords();
			System.out.println(minRec);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}
	@Test
	public void driverMinRecords() {

		// setence coverage 100%

		final Object testingData[][] = {

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard	
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testMinRecords((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testStandarDevRecords(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST STDDEV RECORDS --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Double stddev = this.administratorService.stddevRecords();
			final String print = stddev.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}
	@Test
	public void driverStandardDevRecords() {

		// setence coverage 100%

		final Object testingData[][] = {

			// a) Mostrar información de la dashboard como admin

			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard	
			{
				"member1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testStandarDevRecords((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	//	
	//	@Test
	//	public void driverFindOne() {
	//
	//		System.out.println("--------------------------------TEST FIND ONE ADMIN----------------------------------------------");
	//
	//		final Object testingData[][] = {
	//			//Positive case
	//			{
	//				this.getEntityId("admin"), null, "Find one correct"
	//			},
	//
	//			//Find one admin using another role id
	//			{
	//				this.getEntityId("user1"), IllegalArgumentException.class, "Trying to find an user"
	//			},
	//		};
	//
	//		for (int i = 0; i < testingData.length; i++)
	//			this.findOneTemplate((int) testingData[i][0], (Class<?>) testingData[i][1], (String) testingData[i][2]);
	//	}
	//
	//	protected void findOneTemplate(final int adminId, final Class<?> expected, final String explanation) {
	//
	//		Class<?> caught = null;
	//
	//		try {
	//			this.administratorService.findOne(adminId);
	//
	//		} catch (final Throwable oops) {
	//			caught = oops.getClass();
	//		}
	//
	//		this.checkExceptions(expected, caught);
	//
	//		if (expected == null)
	//			System.out.println("---------------------------- POSITIVE ---------------------------");
	//		else
	//			System.out.println("---------------------------- NEGATIVE ---------------------------");
	//		System.out.println("Explanation: " + explanation);
	//		System.out.println("AdminId: " + adminId);
	//		System.out.println("\r¿Correct? " + (expected == caught));
	//		System.out.println("-----------------------------------------------------------------\r");
	//
	//	}

}
