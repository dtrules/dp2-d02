
package services;

import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Parade;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class ParadeServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private ParadeService		paradeService;

	@Autowired
	private BrotherhoodService	brotherhoodService;

	@Autowired
	private RequestService		requestService;


	//Caso de uso en el que queremos registrar (crear) una parade en el sistema
	protected void createParadeTemplate(final String username, final String titulo, final String descripcion, final Date moment, final Boolean draft, final Class<?> expected) {

		Class<?> caught = null;

		try {
			this.startTransaction();

			super.authenticate(username);

			final Parade proc = this.paradeService.create();

			proc.setDescription(descripcion);
			proc.setDraftMode(draft);
			proc.setMoment(moment);
			proc.setTitle(titulo);

			this.paradeService.save(proc);
			this.paradeService.flush();

			super.unauthenticate();

		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		} finally {
			this.rollbackTransaction();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}

	}

	//Caso de uso en el que actualizamos una parade
	protected void editParadeTemplate(final String username, final int id, final String titulo, final String descripcion, final Date moment, final Boolean draft, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			final Parade proc = this.paradeService.findOne(id);

			proc.setDescription(descripcion);
			proc.setDraftMode(draft);
			proc.setMoment(moment);
			proc.setTitle(titulo);

			this.paradeService.save(proc);
			this.paradeService.flush();

			super.unauthenticate();

		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);

		}
	}

	//Caso de uso en el que se muestra una parade
	protected void showParadeTemplate(final String username, final int procId, final Class<?> expected) {

		Class<?> caught = null;

		try {
			this.paradeService.findOne(procId);

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}

	}

	//Caso en el que queremos listar las parades de una brotherhood
	protected void listParadeByBrotherhoodTemplate(final String username, final int broId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.paradeService.findByBrotherhood(this.brotherhoodService.findOne(broId));

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	//Caso en el que queremos listar las parades
	protected void listParadeTemplate(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.paradeService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	//Caso en el que queremos borrar una parade
	public void deleteParadeTemplate(final String username, final int procId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Buscamos el sponsorship
			final Parade proc = this.paradeService.findOne(procId);

			// Borramos
			this.paradeService.delete(proc);

			// Comprobamos que se ha borrado
			//Assert.isNull(this.paradeService.findOne(procId));

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateParade() {
		//A: Req 10 .5 Creating Processions (parades) 
		final Object testingData[][] = {

			//B: Test positivo
			//C:
			//D:
			{
				"brotherhood1", "titulo1", "descripcion1", new Date(2022, 12, 1, 9, 0), true, null
			},
			//B: Test negativo --> crear una parade con otro actor logueado
			//C:
			//D:
			{
				"member1", "titulo2", "descripcion2", new Date(2022, 12, 1, 9, 0), true, IllegalArgumentException.class
			},
		};

		System.out.println("---CREATE----");

		for (int i = 0; i < testingData.length; i++)
			this.createParadeTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Date) testingData[i][3], (Boolean) testingData[i][4], (Class<?>) testingData[i][5]);
	}

	@Test
	public void driverEditParade() {
		//A: Req 10 .5 Editing Processions (parades) 
		final Object testingData[][] = {

			//B: Test positivo
			//C:
			//D:
			{
				"brotherhood1", 1497, "titulo1", "descripcion1", new Date(2018, 10, 1, 10, 0), true, null
			},
			//B: Test negativo --> editar parade con otro actor logueado
			//C:
			//D:
			{
				"member1", 1498, "titulo2", "descripcion2", new Date(2018, 12, 1, 9, 0), true, IllegalArgumentException.class
			},
			//B: Test negativo --> buscar por una id inexistente
			//C:
			//D:
			{
				"brotherhood1", 4387, "titulo2", "descripcion2", new Date(2018, 12, 1, 9, 0), true, IllegalArgumentException.class
			},

		};

		System.out.println("---EDIT----");

		for (int i = 0; i < testingData.length; i++)
			this.editParadeTemplate((String) testingData[i][0], (int) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Date) testingData[i][4], (Boolean) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	@Test
	public void driverShowParade() {
		//A: Req 10 .5 Showing Processions (parades) 
		final Object testingData[][] = {
			//B: Test positivo
			//C:
			//D:
			{
				"brotherhood1", 1497, null
			},
			//B: Test negativo --> buscar una id inexistente
			//C:
			//D:
			{
				"member1", 4507, IllegalArgumentException.class
			},
		};

		System.out.println("---SHOW----");

		for (int i = 0; i < testingData.length; i++)
			this.showParadeTemplate((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void driverListParade() {
		//A: Req 10 .5 Listing Processions (parades) 
		final Object testingData[][] = {
			// Se accede con brotherhood
			{
				"brotherhood1", null
			},
			// Se accede con member
			{
				"member1", null
			},
			//  Se accede con admin 
			{
				"admin", null
			},

			//  Se accede con usuario no autentificado 
			{
				null, null
			}
		};

		System.out.println("---LIST----");

		for (int i = 0; i < testingData.length; i++)
			this.listParadeTemplate((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverListByBrotherhoodParade() {
		//A: Req 10 .5 Listing Processions (parades) 
		final Object testingData[][] = {
			//B: Test positivo
			//C:
			//D:
			{
				"brotherhood1", 1495, null
			},
			//B: Test positivo
			//C:
			//D:
			{
				"member1", 1496, null
			},
			//B: Test positivo
			//C:
			//D: 
			{
				"admin", 1495, null
			},
			//B: Test positivo
			//C:
			//D:
			{
				null, 1496, null
			},
			//B: Test negativo --> se accede con una id inexistente
			//C:
			//D:
			{
				null, 32234, IllegalArgumentException.class
			}
		};

		System.out.println("---LIST BY BROTHERHOOD----");

		for (int i = 0; i < testingData.length; i++)
			this.listParadeByBrotherhoodTemplate((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void driverDeleteParade() {
		//A: Req 10 .5 Deleting Processions (parades) 
		final Object testingData[][] = {
			//B: Test positivo
			//C:
			//D:
			{
				"brotherhood1", 1498, null
			},
			//B: Test negativo ---> se accede con la id de un objeto que NO esta en draft mode
			//C:
			//D:
			{
				"brotherhood1", 1497, IllegalArgumentException.class
			},
			//B: Test negativo --> se accede con otro actor logueado
			//C:
			//D:
			{
				"admin", 1498, IllegalArgumentException.class
			},
			//B: Test negativo --> se le pasa una id inexistente
			//C:
			//D:
			{
				"brotherhood1", 38572, IllegalArgumentException.class
			},
		};

		System.out.println("---DELETE----");
		for (int i = 0; i < testingData.length; i++)
			this.deleteParadeTemplate((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
