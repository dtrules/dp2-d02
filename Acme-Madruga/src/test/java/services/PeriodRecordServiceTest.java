
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.PeriodRecord;
import domain.Url;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class PeriodRecordServiceTest extends AbstractTest {

	//Service under test ---------------------------

	//B: 77.6% data coverage

	@Autowired
	private PeriodRecordService	periodRecordService;

	@Autowired
	private BrotherhoodService	brotherhoodService;


	//Caso de uso en el que queremos crear nuestro period record

	protected void createPeriodRecordTemplate(final String username, final String title, final String text, final Collection<Url> pictures, final int startYear, final int endYear, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			final PeriodRecord pr = this.periodRecordService.create();

			pr.setTitle(title);
			pr.setText(text);
			pr.setPictures(pictures);
			pr.setStartYear(startYear);
			pr.setEndYear(endYear);

			this.periodRecordService.save(pr);
			this.periodRecordService.flush();

			super.unauthenticate();

		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	//Caso de uso en el que queremos editar nuestro period record

	protected void editPeriodRecordTemplate(final String username, final String title, final String text, final Collection<Url> pictures, final int startYear, final int endYear, final int periodRecordId, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			final PeriodRecord pr = this.periodRecordService.findOne(periodRecordId);

			pr.setTitle(title);
			pr.setText(text);
			pr.setPictures(pictures);
			pr.setStartYear(startYear);
			pr.setEndYear(endYear);

			this.periodRecordService.save(pr);
			this.periodRecordService.flush();

			super.unauthenticate();

		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	//Caso en el que queremos listar los period records
	protected void listPeriodRecordTemplate(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.periodRecordService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}

	}

	//Caso en el que queremos borrar una posicion
	public void deletePeriodRecordTemplate(final String username, final int periodRecordId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Buscamos el periodRecord
			final PeriodRecord pr = this.periodRecordService.findOne(periodRecordId);

			// Borramos
			this.periodRecordService.delete(periodRecordId);

			// Comprobamos que se ha borrado
			//Assert.isNull(this.positionService.findOne(posId));

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreatePeriodRecord() {

		final Collection<Url> pictures = new ArrayList<Url>();
		final Url test = new Url();
		test.setLink("https://www.instagram.com/logo");
		pictures.add(test);

		final Object testingData[][] = {

			// Crear period record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Titletest1", "Texttest1", pictures, 1999, 2012, null
			},
			// Crear period record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", pictures, 2012, 2012, null
			},
			// Crear period record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "asdasdad", "sfsafdsfsafs", pictures, 2014, 2019, null
			},
			// Crear period record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", pictures, 2012, 2015, null
			},
			// Crear period record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Title3333", "sfsafdsfsafs", pictures, 2005, 2011, null
			},
			// Crear period record con datos invalidos -> false
			{
				"brotherhood1", null, "asdasd", null, 2004, 2011, IllegalArgumentException.class
			},
			// Crear period record con datos invalidos -> false
			{
				"brotherhood2", "asdad", "asdad", pictures, 2012, 2011, IllegalArgumentException.class
			},
			// Crear period record con otro usuario -> false
			{
				"member1", "Title3333", "sfsafdsfsafs", pictures, 2005, 2011, IllegalArgumentException.class
			},
			// Crear period record con otro usuario -> false
			{
				"admin", "Title3333", "sfsafdsfsafs", pictures, 2005, 2011, IllegalArgumentException.class
			},
			// Crear period record con otro usuario -> false
			{
				"member2", "Title3333", "sfsafdsfsafs", pictures, 2005, 2011, IllegalArgumentException.class
			},
		};

		System.out.println("---CREATE----");

		for (int i = 0; i < testingData.length; i++)
			this.createPeriodRecordTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Collection<Url>) testingData[i][3], (int) testingData[i][4], (int) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	@Test
	public void driverEditPeriodRecord() {

		final Collection<Url> pictures = new ArrayList<Url>();
		final Url test = new Url();
		test.setLink("https://www.instagram.com/logo");
		pictures.add(test);

		final Object testingData[][] = {

			// Editar period record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Titletest1", "Texttest1", pictures, 1999, 2012, 1481, null
			},
			// Editar period record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", pictures, 2012, 2012, 1483, null
			},
			// Editar period record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "asdasdad", "sfsafdsfsafs", pictures, 2014, 2019, 1481, null
			},
			// Editar period record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", pictures, 2012, 2015, 1483, null
			},
			// Editar period record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Title3333", "sfsafdsfsafs", pictures, 2005, 2011, 1481, null
			},
			// Editar period record con datos invalidos -> false
			{
				"brotherhood1", null, "asdad", null, 2005, 2011, 1481, IllegalArgumentException.class
			},
			// Editar period record con datos invalidos -> false
			{
				"brotherhood2", "asdad", "asdad", pictures, 2012, 2011, 1483, IllegalArgumentException.class
			},
			// Editar period record con otro usuario -> false
			{
				"member1", "Title3333", "sfsafdsfsafs", pictures, 2005, 2011, 1481, IllegalArgumentException.class
			},
			// Editar period record con otro usuario -> false
			{
				"admin", "Title3333", "sfsafdsfsafs", pictures, 2005, 2011, 1481, IllegalArgumentException.class
			},
			// Editar period record con otro usuario -> false
			{
				"member2", "Title3333", "sfsafdsfsafs", pictures, 2005, 2011, 1481, IllegalArgumentException.class
			},
			// Editar period record con otro usuario -> false
			{
				"brotherhood2", "asdad", "asdad", pictures, 2011, 2018, 1481, IllegalArgumentException.class
			},
		};

		System.out.println("---EDIT----");

		for (int i = 0; i < testingData.length; i++)
			this.editPeriodRecordTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Collection<Url>) testingData[i][3], (int) testingData[i][4], (int) testingData[i][5], (int) testingData[i][6],
				(Class<?>) testingData[i][7]);
	}

	@Test
	public void driverListPeriodRecord() {
		final Object testingData[][] = {
			// Se accede con brotherhood -> true
			{
				"brotherhood1", null
			},
			// Se accede con member -> true
			{
				"member1", null
			},
			//  Se accede con admin -> true
			{
				"admin", null
			},

			//  Se accede con usuario no autentificado -> true
			{
				null, null
			}
		};
		System.out.println("---LIST----");
		for (int i = 0; i < testingData.length; i++)
			this.listPeriodRecordTemplate((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverDeletePosition() {
		final Object testingData[][] = {
			// Se accede con brotherhood y datos validos -> true
			{
				"brotherhood1", 1481, null
			},
			// Se accede con brotherhood y datos validos -> true
			{
				"brotherhood2", 1483, null
			},
			//  Se accede con otro actor logueado -> false
			{
				"member1", 1481, IllegalArgumentException.class
			},
			//  Se accede con otro actor logueado -> false
			{
				"admin", 1481, IllegalArgumentException.class
			},
			// Se da una id de otro brotherhood
			{
				"brotherhood1", 1483, IllegalArgumentException.class
			},
			// Se da una id de un period record de otro brotherhood
			{
				"brotherhood2", 1481, IllegalArgumentException.class
			},
		};
		System.out.println("---DELETE----");
		for (int i = 0; i < testingData.length; i++)
			this.deletePeriodRecordTemplate((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
