/*
 * AdministratorServiceTest.java
 * 
 * Copyright (C) 2019 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Member;

@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class MemberServiceTest extends AbstractTest {

	// System under test ------------------------------------------------------

	@Autowired
	private MemberService	memberService;

	@Autowired
	private ActorService	actorService;


	//Supporting services -----------------------------------------------------

	// Tests ------------------------------------------------------------------

	// The following are fictitious test cases that are intended to check that 
	// JUnit works well in this project.  Just righ-click this class and run 
	// it using JUnit.

	@Test
	public void driverCreate() {

		final Object testingData[][] = {
			//Positive test
			{
				null, "Create correcto"
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCreate((Class<?>) testingData[i][0]);
	}

	protected void templateCreate(final Class<?> expected) {

		Class<?> caught = null;

		try {
			this.memberService.create();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	//D: 100% data coverage

	@Test
	public void driverSave() {

		//A: 11. An actor who is authenticated as a member must be able to:
		//C: 100 % sentence coverage

		final Object testingData[][] = {

			//Positive test
			{
				super.getEntityId("member1"), "NAME", "MIDDLENAME", "SURNAME", "ADDRESS", "627027569", "pepe@gmail.com", null, "test positivo"
			},
			//email null
			{
				super.getEntityId("member1"), "NAME", "MIDDLENAME", "SURNAME", "ADDRESS", "627027569", null, ConstraintViolationException.class, "Se intenta guardar con el email en nulo"
			},
			//no email
			{
				super.getEntityId("member2"), "NAME", "MIDDLENAME", "SURNAME", "ADDRESS", "627027569", "pepegmail.com", ConstraintViolationException.class, "Se intenta guardar sin email, rellenando la opción pero sin el arroba"
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateSave((Integer) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(Class<?>) testingData[i][7], (String) testingData[i][8]);
	}
	protected void templateSave(final Integer memberId, final String name, final String middleName, final String surnames, final String address, final String phone, final String email, final Class<?> expected, final String explanation) {

		Class<?> caught = null;

		try {

			final Member m = this.memberService.findOne(memberId);
			m.setName(name);
			m.setMiddleName(middleName);
			m.setSurname(surnames);
			m.setAddress(address);
			m.setPhoneNumber(phone);
			m.setEmail(email);
			this.memberService.save(m);
			this.memberService.flush();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

}
