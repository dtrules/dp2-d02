
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Brotherhood;
import domain.InceptionRecord;
import domain.Url;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class InceptionRecordServiceTest extends AbstractTest {

	//Service under test ---------------------------

	//D) data coverage 68.6%

	@Autowired
	private InceptionRecordService	inceptionRecordService;

	@Autowired
	private BrotherhoodService		brotherhoodService;


	//Caso de uso en el que queremos editar nuestro inception record

	protected void editInceptionRecordTemplate(final String username, final String title, final String text, final Collection<Url> pictures, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			final Brotherhood principal = this.brotherhoodService.findByPrincipal();

			final InceptionRecord ir = principal.getHistory().getInceptionRecord();

			ir.setTitle(title);
			ir.setText(text);
			ir.setPictures(pictures);

			this.inceptionRecordService.save(ir);
			this.inceptionRecordService.flush();

			super.unauthenticate();

		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	//Caso de uso en el que se muestra un inception record
	protected void showInceptionRecordTemplate(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {
			super.authenticate(username);

			final Brotherhood principal = this.brotherhoodService.findByPrincipal();

			this.inceptionRecordService.findOne(principal.getHistory().getInceptionRecord().getId());

			super.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}

	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverEditInceptionRecord() {

		final Collection<Url> pictures = new ArrayList<Url>();
		final Url test = new Url();
		test.setLink("https://www.instagram.com/logo");
		pictures.add(test);

		final Object testingData[][] = {

			// Editar inception record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Titletest1", "Texttest1", pictures, null
			},
			// Editar inception record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", pictures, null
			},
			// Editar inception record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "asdasdad", "sfsafdsfsafs", pictures, null
			},
			// Editar inception record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "test23333", "2323423423", pictures, null
			},
			// Editar inception record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "test23333", "2323423423", pictures, null
			},
			// Editar inception record con datos invalidos -> false
			{
				"brotherhood1", "", "", null, IllegalArgumentException.class
			},
			// Editar inception record con datos invalidos -> false
			{
				"brotherhood1", null, "asdasdsadf", pictures, IllegalArgumentException.class
			},
			// Editar inception record con otro usuario -> false
			{
				"member1", "test", "asdasdsadf", pictures, IllegalArgumentException.class
			},
			// Editar inception record con otro usuario -> false
			{
				"admin", "test", "asdasdsadf", pictures, IllegalArgumentException.class
			},
			// Editar inception record con otro usuario -> false
			{
				"member2", "test", "asdasdsadf", pictures, IllegalArgumentException.class
			},
		};

		System.out.println("---EDIT----");

		for (int i = 0; i < testingData.length; i++)
			this.editInceptionRecordTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Collection<Url>) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void driverShowInceptionRecord() {

		final Object testingData[][] = {
			//Muestra el inception record del brotherhood -> true
			{
				"brotherhood1", null
			},
			//Muestra el inception record del brotherhood -> true
			{
				"brotherhood2", null
			},
			//Muestra el inception record de otro actor -> false
			{
				"member1", IllegalArgumentException.class
			},
			//Muestra el inception record de otro actor -> false
			{
				"member2", IllegalArgumentException.class
			},
			//Muestra el inception record de otro actor -> false
			{
				"admin", IllegalArgumentException.class
			},
		};

		System.out.println("---SHOW----");

		for (int i = 0; i < testingData.length; i++)
			this.showInceptionRecordTemplate((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

}
