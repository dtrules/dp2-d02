
package services;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.LinkRecord;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class LinkRecordServiceTest extends AbstractTest {

	//Service under test ---------------------------

	@Autowired
	private LinkRecordService	linkRecordService;

	@Autowired
	private BrotherhoodService	brotherhoodService;


	//Caso de uso en el que queremos crear nuestro link record

	// D) Data coverage 65.9%

	protected void createLinkRecordTemplate(final String username, final String title, final String text, final String URL, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			final LinkRecord lr = this.linkRecordService.create();

			lr.setTitle(title);
			lr.setText(text);
			lr.setURL(URL);

			this.linkRecordService.save(lr);
			this.linkRecordService.flush();

			super.unauthenticate();

		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	//Caso de uso en el que queremos editar nuestro link record

	protected void editLinkRecordTemplate(final String username, final String title, final String text, final String URL, final int linkRecordId, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			final LinkRecord lr = this.linkRecordService.findOne(linkRecordId);

			lr.setTitle(title);
			lr.setText(text);
			lr.setURL(URL);

			this.linkRecordService.save(lr);
			this.linkRecordService.flush();

			super.unauthenticate();

		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	//Caso en el que queremos listar los link records
	protected void listLinkRecordTemplate(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.linkRecordService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}

	}

	//Caso en el que queremos borrar una posicion
	public void deleteLinkRecordTemplate(final String username, final int linkRecordId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Buscamos el miscellaneousRecord
			final LinkRecord lr = this.linkRecordService.findOne(linkRecordId);

			// Borramos
			this.linkRecordService.delete(linkRecordId);

			// Comprobamos que se ha borrado
			//Assert.isNull(this.positionService.findOne(posId));

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateLinkRecord() {

		//c)sentence coverate 100%

		final Object testingData[][] = {

			// Crear link record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Titletest1", "Texttest1", "http://url.com", null
			},
			// Crear link record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", "http://url.com", null
			},
			// Crear link record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "asdasdad", "sfsafdsfsafs", "http://url.com", null
			},
			// Crear link record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", "http://url.com", null
			},
			// Crear link record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Title3333", "sfsafdsfsafs", "http://url.com", null
			},
			// Crear link record con datos invalidos -> false
			{
				"brotherhood1", "", "", "", ConstraintViolationException.class
			},
			// Crear link record con datos invalidos -> false
			{
				"brotherhood2", "asdad", "asdad", "", ConstraintViolationException.class
			},
			// Crear link record con otro usuario -> false
			{
				"member1", "Title3333", "sfsafdsfsafs", "http://url.com", IllegalArgumentException.class
			},
			// Crear link record con otro usuario -> false
			{
				"admin", "Title3333", "sfsafdsfsafs", "http://url.com", IllegalArgumentException.class
			},
			// Crear link record con otro usuario -> false
			{
				"member2", "Title3333", "sfsafdsfsafs", "http://url.com", IllegalArgumentException.class
			},
		};

		System.out.println("---CREATE----");

		for (int i = 0; i < testingData.length; i++)
			this.createLinkRecordTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void driverEditLinkRecord() {

		final Object testingData[][] = {

			// Editar link record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Titletest1", "Texttest1", "http://url.com", 1487, null
			},
			// Editar link record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", "http://url.com", 1488, null
			},
			// Editar link record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "asdasdad", "sfsafdsfsafs", "http://url.com", 1487, null
			},
			// Editar link record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood2", "Titletest2", "Texttest2", "http://url.com", 1488, null
			},
			// Editar link record logueado como brotherhood y con datos validos -> true
			{
				"brotherhood1", "Title3333", "sfsafdsfsafs", "http://url.com", 1487, null
			},
			// Editar link record con datos invalidos -> false
			{
				"brotherhood1", "", "", "", 1487, ConstraintViolationException.class
			},
			// Editar link record con datos invalidos -> false
			{
				"brotherhood2", "asdad", "asdad", "url", 1488, ConstraintViolationException.class
			},
			// Editar link record con otro usuario -> false
			{
				"member1", "Title3333", "sfsafdsfsafs", "http://url.com", 23, IllegalArgumentException.class
			},
			// Editar link record con otro usuario -> false
			{
				"admin", "Title3333", "sfsafdsfsafs", "http://url.com", 23, IllegalArgumentException.class
			},
			// Editar link record con otro usuario -> false
			{
				"member2", "Title3333", "sfsafdsfsafs", "http://url.com", 23, IllegalArgumentException.class
			},
			// Editar link record con otro brotherhood -> false
			{
				"brotherhood2", "asdad", "asdad", "http://url.com", 1487, IllegalArgumentException.class
			},
		};

		System.out.println("---EDIT----");

		for (int i = 0; i < testingData.length; i++)
			this.editLinkRecordTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (int) testingData[i][4], (Class<?>) testingData[i][5]);
	}

	@Test
	public void driverListLinkRecord() {
		final Object testingData[][] = {
			// Se accede con brotherhood -> true
			{
				"brotherhood1", null
			},
			// Se accede con member -> true
			{
				"member1", null
			},
			//  Se accede con admin -> true
			{
				"admin", null
			},

			//  Se accede con usuario no autentificado -> true
			{
				null, null
			}
		};
		System.out.println("---LIST----");
		for (int i = 0; i < testingData.length; i++)
			this.listLinkRecordTemplate((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverDeleteLinkRecords() {
		final Object testingData[][] = {
			// Se accede con brotherhood y datos validos de su id -> true
			{
				"brotherhood1", 1487, null
			},
			// Se accede con brotherhood y datos validos de su id -> true
			{
				"brotherhood2", 1488, null
			},
			//  Se accede con otro actor logueado -> false
			{
				"member1", 386, IllegalArgumentException.class
			},
			//  Se accede con otro actor logueado -> false
			{
				"admin", 388, IllegalArgumentException.class
			},
			// Se da una id de un link record de otro brotherhood
			{
				"brotherhood2", 1487, IllegalArgumentException.class
			},
		};
		System.out.println("---DELETE----");
		for (int i = 0; i < testingData.length; i++)
			this.deleteLinkRecordTemplate((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
