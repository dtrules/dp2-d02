
package services;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import utilities.AbstractTest;
import domain.Brotherhood;
import domain.Enrol;

@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class EnrolServiceTest extends AbstractTest {

	//SUT -----------------------------------------------------------

	//D) Data coverate 100%

	@Autowired
	private EnrolService		enrolService;

	@Autowired
	private BrotherhoodService	brotherhoodService;

	@Autowired
	private MemberService		memberService;


	// Tests de requisito 11. An actor who is authenticated as a member must be able to:
	//enrol to a brotherhood

	//test positivo de un member que se enrola en una brotherhood
	@Test
	public void testEnrolBrotherhood() {
		int brotherhoodId;
		Enrol enrol;

		brotherhoodId = super.getEntityId("brotherhood1");
		super.authenticate("member1");

		enrol = this.enrolService.create();
		enrol.setBrotherhood(this.brotherhoodService.findOne(brotherhoodId));
		enrol.setMember(this.memberService.findByPrincipal());
		super.unauthenticate();

	}
	//test negativo en el que lo que se intenta enrolar es un usuario no authenticado a una brotherhood
	@Test(expected = IllegalArgumentException.class)
	public void testEnrolBrotherhoodNotAuthenticated() {
		int brotherhoodId;
		Enrol enrol;

		brotherhoodId = super.getEntityId("brotherhood1");
		super.authenticate(null);
		enrol = this.enrolService.create();
		enrol.setBrotherhood(this.brotherhoodService.findOne(brotherhoodId));
		enrol.setMember(this.memberService.findByPrincipal());
		super.unauthenticate();

	}
	//test negativo en el que lo que se intenta enrolar es una bortherhood a una bortherhood

	@Test(expected = IllegalArgumentException.class)
	public void testEnrolBrotherhoodBrotherhood() {
		int brotherhoodId;
		Enrol enrol;

		brotherhoodId = super.getEntityId("brotherhood1");
		super.authenticate("brotherhood2");
		enrol = this.enrolService.create();
		enrol.setBrotherhood(this.brotherhoodService.findOne(brotherhoodId));
		enrol.setMember(this.memberService.findByPrincipal());
		super.unauthenticate();

	}

	protected void testListEnrols(final int brotherhoodId, final Class<?> expected) {

		Class<?> caught = null;

		System.out.println("------------------------------------TEST LIST ENROLS (MEMBERS) OF A BROTHERHOOD --------------------------------------------------");

		try {
			final Brotherhood bro = this.brotherhoodService.findOne(brotherhoodId);

			super.authenticate(bro.getUsername());

			@SuppressWarnings("unused")
			final Collection<Enrol> enrols = bro.getEnrols();

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverListEnrols() {

		final Object testingData[][] = {

			//Positive test
			{
				super.getEntityId("brotherhood1"), null,
			},
			//email null
			{
				super.getEntityId("member1"), IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testListEnrols((Integer) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	//DELETE ENROL

	//Test de todos los requisitos refentes a borrar enrls
	@Test
	public void driverDeleteEnrol() {

		//sentence coverage 100%

		System.out.println("-----------------------------------TEST DELETE ENROL---------------------------------------------");

		final Object testingData[][] = {
			//Positive test. 10. An actor who is authenticated as a brotherhood must be able to:
			// 3. Manage the members of the brotherhood, which includes listing, showing, enrolling, and removing them. 
			//When a member is enrolled, a position must be selected by the brotherhood.
			{
				"brotherhood1", 1499, null
			},

			//Try to delete an enrol being anonymous. Cant delete if is anonymous
			{
				null, 1500, IllegalArgumentException.class
			},

			//Try to delete an enrol as a member. Cant delete as a member
			{
				"member1", 1499, IllegalArgumentException.class
			},

			//Try to delete a non persisted enrol.
			{
				"brotherhood1", this.enrolService.create().getId(), IllegalArgumentException.class
			},
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateDeleteEnrol((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	protected void templateDeleteEnrol(final String username, final int entityId, final Class<?> expected) {

		Class<?> caught = null;
		Enrol res = null;
		this.authenticate(username);

		try {
			res = this.enrolService.findOne(entityId);
			this.enrolService.delete(res);

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.unauthenticate();
		this.checkExceptions(expected, caught);
		if (expected == null)
			System.out.println("---------------------------- POSITIVE ---------------------------");
		else
			System.out.println("---------------------------- NEGATIVE ---------------------------");

	}
}
