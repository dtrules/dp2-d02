
package services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import utilities.AbstractTest;
import domain.Parade;
import domain.Request;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@Transactional
public class RequestServiceTest extends AbstractTest {

	@Autowired
	private RequestService	requestService;

	@Autowired
	private MemberService	memberService;

	@Autowired
	private ParadeService	paradeService;


	/*
	 * a)
	 * 11.1 11. An actor who is authenticated as a member must be able to:
	 * 1. Manage his or her requests to march on a procession, which includes listing them by status, showing, creating them, and deleting them. Note that the requests cannot be updated, but they can be deleted as long as they are in the pending status.
	 * 
	 * 
	 * 
	 * d) 100% data coverage
	 */

	// c)  100% sentence coverage
	@Test
	public void driverCreate() {

		final Object testingData[][] = {
			//Test positivo de crear un request como member
			{
				"member1", null
			}

			//  b) Test negativo de crear un request. Solo un usuario autentificado como member puede crear un request
			, {
				"brotherhood1", IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCreate((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void templateCreate(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {
			this.authenticate(username);

			final Request request = this.requestService.create();

			request.setMember(this.memberService.findByPrincipal());
			final Parade parade = this.paradeService.findOne(super.getEntityId("parade1"));
			request.setParade(parade);

			this.requestService.save(request);
			this.requestService.flush();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		super.checkExceptions(expected, caught);
	}

	// c) 97.6% sentence coverage 
	@Test
	public void driverDelete() {

		final Object testingData[][] = {

			//Test positivo para borrar un request
			{
				"request3", null
			}, {
				// b) Test negativo para borrar un request. No se debe de podre borrar un request que no esta como pending
				"request1", IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateDelete((String) testingData[i][0], (Class<?>) testingData[i][1]);

	}

	protected void templateDelete(final String request, final Class<?> expected) {
		Class<?> caught = null;

		try {

			final Integer requestId = super.getEntityId(request);

			this.requestService.delete(requestId);

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		super.checkExceptions(expected, caught);
	}

}
