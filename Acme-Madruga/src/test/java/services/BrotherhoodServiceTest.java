/*
 * AdministratorServiceTest.java
 * 
 * Copyright (C) 2019 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Brotherhood;

@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class BrotherhoodServiceTest extends AbstractTest {

	//D) data coverage 10%

	// System under test ------------------------------------------------------

	@Autowired
	private BrotherhoodService	brotherhoodService;

	@Autowired
	private ActorService		actorService;


	//Supporting services -----------------------------------------------------

	// Tests ------------------------------------------------------------------

	// The following are fictitious test cases that are intended to check that 
	// JUnit works well in this project.  Just righ-click this class and run 
	// it using JUnit.

	@Test
	public void driverCreate() {

		final Object testingData[][] = {
			//Crear un actor de Brotherhood
			{
				null, "Create correcto"
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.templateCreate((Class<?>) testingData[i][0]);
	}

	protected void templateCreate(final Class<?> expected) {

		Class<?> caught = null;

		try {
			this.brotherhoodService.create();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverSave() {

		// sentence coverage 100%

		final Object testingData[][] = {

			// a) Guardar los datos de un brotherhood
			{
				super.getEntityId("brotherhood1"), "NAME", "MIDDLENAME", "SURNAME", "ADDRESS", "627027569", "pepe@gmail.com", null, "test positivo"
			},
			// a) Guardar los datos de un brotherhood
			// b) Para cada actor, el sistema debe almacenar un nombre, .... , un email.
			{
				super.getEntityId("brotherhood1"), "NAME", "MIDDLENAME", "SURNAME", "ADDRESS", "627027569", null, ConstraintViolationException.class, "Se intenta guardar con el email en nulo"
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.templateSave((Integer) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(Class<?>) testingData[i][7], (String) testingData[i][8]);
	}
	protected void templateSave(final Integer brotherhoodId, final String name, final String middleName, final String surnames, final String address, final String phone, final String email, final Class<?> expected, final String explanation) {

		Class<?> caught = null;

		try {

			final Brotherhood m = this.brotherhoodService.findOne(brotherhoodId);
			m.setName(name);
			m.setMiddleName(middleName);
			m.setSurname(surnames);
			m.setAddress(address);
			m.setPhoneNumber(phone);
			m.setEmail(email);
			this.brotherhoodService.save(m);
			this.brotherhoodService.flush();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	protected void ListBrotherhoodBelongsTemplate(final String username, final int memberId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.brotherhoodService.findAllMemberBelongs(memberId);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}
	@Test
	public void driverListBrotherhoodsBelongs() {

		final Object testingData[][] = {

			// a) Listar todas las brotherhoods a las que pertenece un member
			{
				"member1", 1491, null,
			},
			// a) Listar todas las brotherhoods a las que pertenece un member
			// b) Un actor autenticado como member debe ser capaz de ver a todas las brotherhood que pertenece
			{
				"brotherhood1", 0, IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.ListBrotherhoodBelongsTemplate((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	protected void ListBrotherhoodBelongedTemplate(final String username, final int memberId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.brotherhoodService.findAllMemberBelonged(memberId);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);

		if (expected == null && caught == null)
			System.out.println("---Los datos son correctos como era esperado----");
		else if (expected != null && caught != null && expected.equals(caught)) {
			System.out.println("---Los datos son incorrectos o violan las restricciones como era esperado----");
			final String excepcion = expected.toString();
			System.out.println(excepcion);
		}
	}
	@Test
	public void driverListBrotherhoodsBelonged() {

		final Object testingData[][] = {

			//Id valido
			{
				"brotherhood1", 1491, null,
			},
			//id inexistente
			{
				"brotherhood1", 0, IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.ListBrotherhoodBelongedTemplate((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
